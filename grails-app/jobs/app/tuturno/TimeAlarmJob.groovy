package app.tuturno

class TimeAlarmJob {
	
	def group = "TimeAlarmGroup"
	def timeAlarmService
	
    static triggers = {
      cron cronExpression: "0 * * * * ?" // execute every minute
    }

    def execute() {
		timeAlarmService.processTimeAlarms(TimeAlarm.executeQuery("select ta from TimeAlarm as ta inner join ta.turn as t where ta.enabled = true and t.status = 'pending'"))
    }
}
