package app.tuturno

abstract class Alarm {
	
	Boolean fired = false
	Boolean enabled = true
	
	static belongsTo = [turn: Turn]

    static constraints = {
		turn()
		fired()
		enabled()
    }
}
