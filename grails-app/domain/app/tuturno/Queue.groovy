package app.tuturno

class Queue {

	String code
	String name
    String prefix
	
	static belongsTo = [company: Company]

    static constraints = {
    	company()
		code(unique: 'company')
    	name()
    	prefix()
    }

    String toString() {
    	"$name ($company)"
    }
}
