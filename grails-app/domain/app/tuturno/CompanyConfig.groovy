package app.tuturno

class CompanyConfig {

	String name
	Boolean active

	static hasMany = [configProperties: ConfigProperty]
	static belongsTo = [company: Company]

    static constraints = {
		name(nullable: true)
    	company()
    	active(validator: { value, obj ->
                if (value) {
					def config = CompanyConfig.findByCompanyAndActive(obj.company, true)
					
					if (config && config.id != obj.id) {
						return ['uniqueActiveConfig']
					}
                }
            })
    }

    String toString() {
    	"$id - $company ($active)"
    }
}
