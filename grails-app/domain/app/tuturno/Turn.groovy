package app.tuturno

abstract class Turn {

    Date dateCreated
    String status = 'pending'
	String statusMessage

	static belongsTo = [queue: Queue, subscriber: Subscriber]
	static hasMany = [alarms: Alarm]

    static constraints = {
    	queue(index: 'queue_subscriber_idx')
    	subscriber(index: 'queue_subscriber_idx', nullable: true)
    	status(inList: ['pending', 'attended', 'cancelled'])
		statusMessage(nullable: true)
        dateCreated()
		alarms()
    }
}
