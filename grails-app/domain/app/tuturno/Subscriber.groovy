package app.tuturno

import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber

class Subscriber {

    String phoneNumber
	String phoneOS = 'other'
	String phoneToken

	String name
	String email
	
	String status = "inactive"
	boolean activeNotifications = false

    static constraints = {
    	phoneNumber(unique: true, validator: { value ->
                try {
                    PhoneNumberUtil phoneUtils = PhoneNumberUtil.instance
                    if (!phoneUtils.isValidNumber(phoneUtils.parse(value, 'CO'))) {
                        return ['invalidPhoneNumber']
                    }
                } catch(NumberParseException ex) {
                    return ['invalidPhoneNumber']
                }
            })
    	name(nullable: true)
    	email(nullable: true)
    	phoneOS(nullable: true, inList:['android', 'ios', 'other'])
    	phoneToken(nullable: true)
    	status(inList:['inactive', 'active'])
		activeNotifications()
    }

    String toString() {
        "$phoneNumber - $phoneOS ($name)"
    }
}
