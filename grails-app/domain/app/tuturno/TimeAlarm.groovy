package app.tuturno

class TimeAlarm extends Alarm {
	
	Integer minutesLeft

	static constraints = {
		minutesLeft()
	}
}
