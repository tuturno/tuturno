package app.tuturno

class NumericTurn extends Turn {

    Integer number

    static constraints = {
        number()
    }

    String toString() {
    	"$number - $queue ($subscriber)"
    }
}
