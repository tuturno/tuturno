package app.tuturno

class ConfigProperty {

	String name
	String value
	Boolean publicVisible = false

	static belongsTo = [companyConfig: CompanyConfig]

    static constraints = {
    	companyConfig(index: 'property_key_idx')
    	name(index: 'property_key_idx')
    	value()
		publicVisible()
    }

    String toString() {
    	"$name = $value"
    }
}
