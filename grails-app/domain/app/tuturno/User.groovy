
package app.tuturno

class User extends grails.plugin.nimble.core.UserBase {

	// Extend UserBase with your custom values here	
	Company company	
	
	static constraints = {
		company(nullable:true)
	}
	
	String toString() {
    	"$username"
    }

}
