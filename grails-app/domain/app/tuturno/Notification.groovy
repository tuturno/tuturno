package app.tuturno

class Notification {

	Company company
    Date dateCreated
	String uuid
	String type
	String method
	Subscriber receiver
	String data
	String subject
	String message
	String statusCode = 'pending'
	String statusMessage

	static constraints = {
		company()
		dateCreated()
		type()
		method(inList: ['push', 'sms', 'email'])
		statusCode(inList: ['pending', 'sent', 'received', 'failed'])
		receiver()
		data(maxSize: 4096)
		message(nullable: true)
		subject(nullable: true)
		statusMessage(nullable: true)
		uuid(unique: true)
    }
}
