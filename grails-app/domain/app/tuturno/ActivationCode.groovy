package app.tuturno

class ActivationCode {

	Subscriber subscriber
	String code
	
    static constraints = {
		subscriber(unique: true)
		code()
    }
}
