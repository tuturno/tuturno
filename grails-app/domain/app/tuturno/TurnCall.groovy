package app.tuturno

class TurnCall {

	Integer number
	Turn turn
	Date dateCreated
	Boolean jumpElapsedTimeCalculation = false

	static belongsTo = [queue: Queue]

    static constraints = {
    	dateCreated()
    	queue()
		number()
    	turn(nullable: true)
		jumpElapsedTimeCalculation(nullable: true)
    }

    String toString() {
        "$queue - $number ($dateCreated)"
    }
}