package app.tuturno

class FixedTimeTurn extends Turn {

	Date datetime

    static constraints = {
        datetime(attributes: [precision: 'minute'])
    }

    String toString() {
    	"$datetime - $queue ($subscriber)"
    }
}
