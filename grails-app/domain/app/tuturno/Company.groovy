package app.tuturno

class Company {

	String code
	String name

	static hasMany = [queues: Queue]
	static hasOne = [user: User]

    static constraints = {
    	name()
		code(unique: true)
    	queues()
    }

    String toString() {
    	name
    }
}
