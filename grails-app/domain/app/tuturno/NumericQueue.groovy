package app.tuturno

class NumericQueue extends Queue {

	Integer lastAssignedTurn = 0
	Integer currentTurn = 0
	Integer maxTurn
	Integer turnCallsTimeLapseAverage = 0
	Date lastTurnCallDatetime
	Boolean jumpNextElapsedTimeCalculation = true
	
    static constraints = {
		code(unique: 'company')
		lastAssignedTurn()
		currentTurn()
		maxTurn()
		turnCallsTimeLapseAverage()
		lastTurnCallDatetime(nullable: true, attributes: [precision: 'minute'])
		jumpNextElapsedTimeCalculation(nullable: true)
    }
}
