package app.tuturno

import grails.transaction.Transactional

@Transactional
class NumericQueueService {
	
	def turnsService

    def reset(NumericQueue queue) {
		if (queue) {
			queue.lastAssignedTurn = 0
			queue.currentTurn = 0
			queue.jumpNextElapsedTimeCalculation = true
			
			turnsService.cancelPendingTurns(queue, 'La fila comienza de nuevo')
		}
    }
}
