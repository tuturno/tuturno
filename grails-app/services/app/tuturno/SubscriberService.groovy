package app.tuturno

import grails.converters.JSON
import grails.transaction.Transactional

import org.springframework.integration.support.MessageBuilder

@Transactional
class SubscriberService {
	
	def grailsApplication
	def companyService
	def notificationChannel

	def saveOrUpdate(Subscriber subscriber, Integer queueId = null) {
		def existingSubscriber = Subscriber.findByPhoneNumber(subscriber.phoneNumber)

		if (subscriber.hasErrors() && !existingSubscriber) {
			return subscriber
		}

		if (subscriber.id) {
			subscriber.save()
		} else {
			if (existingSubscriber) {
				existingSubscriber.properties = subscriber.properties
				subscriber = existingSubscriber
			} else {
				subscriber.save(flush: true)
				
				if (subscriber.phoneOS == 'other') {
					def sendNotification = true
					
					if (queueId) {
						def queue = Queue.findById(queueId)
						def notificationDisabled = companyService.getProperty(queue?.company, "notifications.WelcomeSubscriber.disabled")?.value
						sendNotification = notificationDisabled == null || notificationDisabled != 'true'
					}
					
					if (sendNotification) {
						sendWelcomeNotification(subscriber)
					}
				}
			}
		}

		return subscriber
	}

	def sendWelcomeNotification(subscriber) {
		def uuid = UUID.randomUUID().toString()

		def notification = new Notification(
				company: grailsApplication.config.tuturnoCompany,
				uuid: UUID.randomUUID().toString(),
				receiver: subscriber,
				type: 'WelcomeSubscriber',
				method: 'sms',
				data: ([name: subscriber.name] as JSON).toString())

		notificationChannel.send(MessageBuilder.withPayload(notification).build())
	}
}
