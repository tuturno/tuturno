package app.tuturno

import grails.plugin.nimble.core.UserBase;
import grails.transaction.Transactional
import grails.plugin.nimble.core.Role
import grails.plugin.nimble.core.UserBase

class DeveloperService {

    public static final String DEVELOPER_ROLE = "DEVELOPER"

	def permissionService

	boolean add(UserBase user) {
		// Grant administrative role
		def developerRole = Role.findByName(DeveloperService.DEVELOPER_ROLE)

		if (!developerRole) {
			log.error("Unable to located default developer role")
			throw new RuntimeException("Unable to locate default developer role")
		}

		log.info("Trying to add user [$user.id]:$user.username as developer")

		developerRole.addToUsers(user)
		user.addToRoles(developerRole)

		if (!developerRole.save()) {
			log.error "Unable to grant developer privilege to [$user.id]:$user.username"
			developerRole.errors.each {  log.error '[${user.username}] - ' + it }
			developerRole.discard()
			user.discard()
			return false
		}

		if (!user.save()) {
			log.error "Unable to grant developer role to [$user.id]$user.username failed to modify user account"
			user.errors.each { log.error it }

			throw new RuntimeException("Unable to grant developer role to [$user.id]$user.username")
		}

		// Grant administrative 'ALL' permission
//		Permission adminPermission = new Permission(target:'*')
//		adminPermission.managed = true
//		adminPermission.type = Permission.adminPerm

//		permissionService.createPermission(adminPermission, user)

		log.info "Granted developer privileges to [$user.id]:$user.username"
		return true
	}

	boolean remove(UserBase user) {
		def developerRole = Role.findByName(DeveloperService.DEVELOPER_ROLE)

		if (!developerRole) {
			log.error("Unable to located default developer role")
			throw new RuntimeException("Unable to locate default developer role")
		}

		developerRole.removeFromUsers(user)
		user.removeFromRoles(developerRole)

		if (!developerRole.save()) {
			log.error "Unable to revoke developer privilege from [$user.id]$user.username"
			developerRole.errors.each { log.error it }

			developerRole.discard()
			user.discard()
			return false
		}

		if (!user.save()) {
			log.error "Unable to revoke developer privilege from [$user.id]$user.username failed to modify user account"
			user.errors.each { log.error it }

			throw new RuntimeException("Unable to revoke developer privilege from [$user.id]$user.username failed to modify user account")
		}

		// Revoke administrative 'ALL' permission(s)
//		def permToRemove = []
//		user.permissions.each {
//			if (it.type.equals(AllPermission.name) || it.type.equals(grails.plugin.nimble.auth.AllPermission.name)) {
//				permToRemove.add(it)
//				log.debug("Found $it.type for user [$user.id]$user.username adding to remove queue")
//			}
//		}
//
//		permToRemove.each {
//			user.permissions.remove(it)
//			log.debug("Removing $it.type from user [$user.id]$user.username")
//		}
//
//		if (!user.save()) {
//			log.error "Unable to revoke administration permission from [$user.id]$user.username failed to modify user account"
//			user.errors.each { log.error it }
//
//			throw new RuntimeException("Unable to revoke administration permission from [$user.id]$user.username")
//		}

		log.info "Revoked developer privilege from [$user.id]$user.username"
		return true
	}
}
