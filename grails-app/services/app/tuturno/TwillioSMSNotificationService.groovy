package app.tuturno

import grails.transaction.Transactional

import org.apache.http.message.BasicNameValuePair

import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat
import com.twilio.sdk.TwilioRestClient

@Transactional
class TwillioSMSNotificationService {

	static String ACCOUNT_SID = "ACfa47cc15c3e4a26c970643888c770ab4";
	static String AUTH_TOKEN = "99bf208bfb6750191b4efa67438ebfd1";
	static String SENDER_PHONE_NUMBER = "+16502764232";

	def companyService

	def client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
	def messageFactory = client.getAccount().getMessageFactory();

	def sendNotification(notification) {
		if (notification.statusCode != 'pending') {
			return [id: notification.id, status: notification.statusCode, message: notification.statusMessage]
		}

		def phoneUtils = PhoneNumberUtil.instance
		def phoneNumber = phoneUtils.format(phoneUtils.parse(notification.receiver.phoneNumber, 'CO'), PhoneNumberFormat.E164)

		// Build a filter for the MessageList
		def params = new ArrayList<BasicNameValuePair>();
		params.add(new BasicNameValuePair("Body", notification.message));
		params.add(new BasicNameValuePair("To", phoneNumber));
		params.add(new BasicNameValuePair("From", SENDER_PHONE_NUMBER));

		def resp

		if (companyService.getPropertyConfig(notification.company, "notifications.sms.mock")?.value == 'true') {
			resp = [
				sid: 1
			]
		} else {
			resp = messageFactory.create(params);
		}

		[id: notification.id, status: (resp.sid ? 'sent' : 'failed')]
	}
}
