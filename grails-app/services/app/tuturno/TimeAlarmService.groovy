package app.tuturno

import grails.converters.JSON
import grails.transaction.Transactional

import org.springframework.integration.support.MessageBuilder

@Transactional
class TimeAlarmService {

	def turnsService
	def companyService

	def notificationChannel

	def processTimeAlarms(timeAlarms) {
		def currentTime = Calendar.instance
		def alarmsToFire = [:]

		timeAlarms.each { alarm ->
			if (alarm.turn instanceof NumericTurn && alarm.turn.status == 'pending' && alarm.enabled) {
				def turnsLeft = turnsService.calculateRemainingTurns(alarm.turn)
				def timeLeft = turnsLeft * alarm.turn.queue.turnCallsTimeLapseAverage

				def turnTime = currentTime.clone()
				turnTime.add(Calendar.SECOND, timeLeft)
				turnTime.add(Calendar.MINUTE, -alarm.minutesLeft)

				def alarmTimeDiff = turnTime.timeInMillis - currentTime.timeInMillis
				alarmTimeDiff = alarmTimeDiff / 1000 / 60

				if (alarm.fired) {
					// Could experiment to reactivate alarm when alarmTimeDiff greater than a % value
					if (alarmTimeDiff > 0) {
						alarm.fired = false
					}
				} else {
					if (alarmTimeDiff <= 0) {
						if (alarmsToFire[alarm.turn.id]) {
							alarmsToFire[alarm.turn.id] << alarm
						} else {
							alarmsToFire[alarm.turn.id] = [alarm]
						}

						alarm.fired = true
					}
				}
			}
		}

		// Fire the closest alarm to turn to avoid duplicated notifications
		alarmsToFire.each { key, value ->
			def minAlarm = value[0]

			value.each{ alarm ->
				if (alarm.minutesLeft < minAlarm.minutesLeft) {
					minAlarm = alarm
				}
			}

			fireTimeAlarmNotification(minAlarm)
		}
	}

	def fireTimeAlarmNotification(alarm) {
		def turnsLeft = turnsService.calculateRemainingTurns(alarm.turn)
		def timeLeft = turnsLeft * alarm.turn.queue.turnCallsTimeLapseAverage
		def maxTurnDigits = String.valueOf(alarm.turn.queue.maxTurn).length()
		
		def type = 'TimeAlarm'
		def uuid = UUID.randomUUID().toString()
		def companyConfig = companyService.getActiveConfig(alarm.turn.queue.company)
		def dataMap = [companyConfigId: companyConfig.id,
			companyConfigVersion: companyConfig.version,
			companyName: alarm.turn.queue.company.name,
			queue: alarm.turn.queue.id,
			queuePrefix: alarm.turn.queue.prefix,
			turnId: alarm.turn.id,
			uuid: uuid,
			type: type,
			alarmId: alarm.id,
			alarmMinutesLef: alarm.minutesLeft,
			minutesLeft: ((int)timeLeft / 60)]
		
		if (alarm.turn.queue instanceof NumericQueue) {
			dataMap.currentTurn = alarm.turn.queue.currentTurn
			dataMap.formattedCurrentTurn = String.format("%0${maxTurnDigits}d", alarm.turn.queue.currentTurn)
		}
		
		def data = (dataMap as JSON).toString()

		def notification = new Notification(
				company: alarm.turn.queue.company,
				uuid: uuid,
				receiver: alarm.turn.subscriber,
				type: type,
				data: data)

		if (notification.receiver.phoneOS == 'android' || notification.receiver.phoneOS == 'ios') {
			notification.method = 'push'
		} else if (notification.receiver.phoneOS == 'other') {
			notification.method = 'sms'
		}

		notificationChannel.send(MessageBuilder.withPayload(notification).build())
	}
}
