package app.tuturno

import grails.transaction.Transactional

@Transactional
class CompanyService {
	
	def grailsApplication

    def getActiveConfig(company) {
		return CompanyConfig.findByCompanyAndActive(company, true)
    }
	
	def getProperty(company, property) {
		def config = getActiveConfig(company)
		
		if (!config) {
			return null
		}
		
		return ConfigProperty.findByCompanyConfigAndName(config, property)
	}
	
	def getPropertyConfig(company, property) {
		def config = getActiveConfig(company)
		
		if (!config) {
			return null
		}
		
		def propertyConfig = ConfigProperty.findByCompanyConfigAndName(config, property)
		return propertyConfig ? propertyConfig : getProperty(grailsApplication.config.tuturnoCompany, property)
	}
	
	def getPublicVisibleProperties(company) {
		def config = getActiveConfig(company)
		
		if (!config) {
			return null
		}
		
		return ConfigProperty.findAllByCompanyConfigAndPublicVisible(config, true)
	}
}
