package app.tuturno

import grails.transaction.Transactional
import groovy.json.JsonSlurper
import com.notnoop.apns.APNS
import com.notnoop.apns.ApnsNotification
import com.notnoop.apns.ApnsService

@Transactional
class APNNotificationService {

	def apnsService

    def sendNotification(notification) {
		if (notification.statusCode != 'pending') {
			return [id: notification.id, status: notification.statusCode, message: notification.statusMessage]
		}
		
    	def jsonMap = new JsonSlurper().parseText(notification.data)

		def payload = APNS.newPayload().sound('default').alertBody(notification.message)
 		jsonMap.each { key, value ->
 			payload.customField(key, String.valueOf(value))
 		}

        println payload
        def error
        try {
        	apnsService.push(notification.receiver.phoneToken, payload.build())
        }
		catch(ex) {
			error = ex.message
		}
	    
	    [id: notification.id, message: error, status: (error ? 'failed' : 'sent')]
    }
}
