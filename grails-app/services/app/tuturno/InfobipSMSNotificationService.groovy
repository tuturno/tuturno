package app.tuturno

import grails.transaction.Transactional
import grails.util.Environment

import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat

@Transactional
class InfobipSMSNotificationService {

	static String endpoint = 'http://api.infobip.com'
	static String USERNAME = "Jul1ian23"
	static String PASSWORD = "Quiceno02"
	static String SENDER = "TuTurno"
	
	def companyService

	def sendNotification(notification) {
		if (notification.statusCode != 'pending') {
			return [id: notification.id, status: notification.statusCode, message: notification.statusMessage]
		}

		def phoneUtils = PhoneNumberUtil.instance
		def phoneNumber = phoneUtils.format(phoneUtils.parse(notification.receiver.phoneNumber, 'CO'), PhoneNumberFormat.E164)

		def postJson = [
			authentication: [username: USERNAME, password: PASSWORD],
			messages: [
				[
					sender: SENDER,
					text: notification.message,
					recipients: [
						[gsm: phoneNumber]
					]
				]
			]
		]

		def resp = null

		if (companyService.getPropertyConfig(notification.company, "notifications.sms.mock")?.value == 'true') {
			resp = [
				data: [
					results: [[status: '0']]]
			]
		} else {
			withRest(uri: endpoint) {
				resp = post(path : '/api/v3/sendsms/json', body: postJson, requestContentType : groovyx.net.http.ContentType.JSON)
			}
		}

		println notification.message

		[id: notification.id, status: (resp.data?.results[0]?.status == '0' ? 'sent' : 'failed')]
	}
}
