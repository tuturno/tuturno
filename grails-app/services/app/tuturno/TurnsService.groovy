package app.tuturno

import grails.converters.JSON
import grails.transaction.Transactional

import org.springframework.integration.support.MessageBuilder

@Transactional
class TurnsService {

	def companyService
	def alarmService
	def notificationChannel

	def generateNumericTurn(numericTurnInstance) {
		synchronized(numericTurnInstance.queue) {
			def number = TurnUtils.calculateNextTurnToAssign(numericTurnInstance.queue)
			numericTurnInstance.number = number
			numericTurnInstance.queue.lastAssignedTurn = number
			numericTurnInstance.save(flush: true)
		}
	}

	def storeNumericTurn(numericTurnInstance) {
		synchronized(numericTurnInstance.queue) {
			numericTurnInstance.queue.lastAssignedTurn = numericTurnInstance.number
			numericTurnInstance.save(flush: true)
		}
	}

	def notifyFixedTimeTurnRegistration(turn) {
		def type = 'FixedTimeTurnRegistration'
		def uuid = UUID.randomUUID().toString()
		def companyConfig = companyService.getActiveConfig(turn.queue.company)
		def data = ([companyConfigId: companyConfig.id,
			companyConfigVersion: companyConfig.version,
			queue: turn.queue.id,
			turnId: turn.id,
			uuid: uuid,
			type: type,
			datetime: (turn.datetime.time)] as JSON).toString()

		def notification = new Notification(
				company: turn.queue.company,
				uuid: uuid,
				receiver: turn.subscriber,
				type: type,
				data: data)

		if (notification.receiver.phoneOS == 'android' || notification.receiver.phoneOS == 'ios') {
			notification.method = 'push'
		} else if (notification.receiver.phoneOS == 'other') {
			notification.method = 'sms'
		}

		return notification
	}

	def notifyNumericTurnRegistration(turn) {
		def turnsLeft = calculateRemainingTurns(turn)
		def timeLeft = turnsLeft * turn.queue.turnCallsTimeLapseAverage
		def maxTurnDigits = String.valueOf(turn.queue.maxTurn).length()
		
		def type = 'NumericTurnRegistration'
		def uuid = UUID.randomUUID().toString()
		def companyConfig = companyService.getActiveConfig(turn.queue.company)
		def data = ([companyConfigId: companyConfig.id,
			companyConfigVersion: companyConfig.version,
			companyName: turn.queue.company.name,
			queuePrefix: turn.queue.prefix,
			queue: turn.queue.id,
			turnId: turn.id,
			uuid: uuid,
			type: type,
			number: turn.number,
			minutesLeft: ((int)timeLeft / 60),
			formattedNumber: String.format("%0${maxTurnDigits}d", turn.number)] as JSON).toString()

		def notification = new Notification(
				company: turn.queue.company,
				uuid: uuid,
				receiver: turn.subscriber,
				type: type,
				data: data,
				statusCode: 'pending')

		if (notification.receiver.phoneOS == 'android' || notification.receiver.phoneOS == 'ios') {
			notification.method = 'push'
		} else if (notification.receiver.phoneOS == 'other') {
			notification.method = 'sms'
		}

		return notification
	}

	def calculateRemainingTurns(turn) {
		if (turn.number >= turn.queue.currentTurn) {
			return turn.number - turn.queue.currentTurn
		} else {
			return turn.queue.maxTurn - turn.queue.currentTurn + turn.number
		}
	}

	def cancelPendingTurns(queue, reason) {
		def pendingTurns = Turn.findAllByQueueAndStatus(queue, 'pending')
		pendingTurns.each { turn ->
			cancelTurn(turn, reason)
		}
	}
	
	def cancelTurn(turn, reason) {
		if (turn.status == 'pending') {
			turn.status = 'cancelled'
			turn.statusMessage = reason

			notifyTurnCancelation(turn)
		}
	}

	def notifyTurnCancelation(turn) {
		if (!turn.subscriber) {
			return
		}

		def type = 'TurnCancellation'
		def uuid = UUID.randomUUID().toString()
		def companyConfig = companyService.getActiveConfig(turn.queue.company)
		def data = ([companyConfigId: companyConfig.id,
			companyConfigVersion: companyConfig.version,
			companyName: turn.queue.company.name,
			queuePrefix: turn.queue.prefix,
			queue: turn.queue.id,
			turnId: turn.id,
			uuid: uuid,
			type: type,
			reason: turn.statusMessage] as JSON).toString()

		def notification = new Notification(
				company: turn.queue.company,
				uuid: uuid,
				receiver: turn.subscriber,
				type: type,
				data: data,
				statusCode: 'pending')

		if (notification.receiver.phoneOS == 'android' || notification.receiver.phoneOS == 'ios') {
			notification.method = 'push'
		} else if (notification.receiver.phoneOS == 'other') {
			notification.method = 'sms'
		}

		notificationChannel.send(MessageBuilder.withPayload(notification).build())
	}
}
