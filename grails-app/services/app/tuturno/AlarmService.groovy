package app.tuturno

import grails.transaction.Transactional

@Transactional
class AlarmService {
	
	def companyService

    def createDefaultNumericTurnAlarms(turn) {
		def defaultAlarms = companyService.getProperty(turn.queue.company, "alarms.timeAlarms.default")
		
		if (defaultAlarms) {
			def minutes = defaultAlarms.value.replaceAll(' ', '').split(',')
			
			minutes.each { minute ->
				new TimeAlarm(turn: turn, minutesLeft: minute as int).save()
			}
		}
		
		return turn
    }
}
