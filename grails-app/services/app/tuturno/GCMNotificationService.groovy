package app.tuturno

import grails.transaction.Transactional
import groovy.json.JsonSlurper

@Transactional
class GCMNotificationService {

    def androidGcmService

    def sendNotification(notification) {
		if (notification.statusCode != 'pending') {
			return [id: notification.id, status: notification.statusCode, message: notification.statusMessage]
		}
		
    	def jsonMap = new JsonSlurper().parseText(notification.data)

		def payload = [:]
 		jsonMap.each { key, value ->
 			payload[key] = String.valueOf(value)
 		}
        
        println payload
        def response = androidGcmService.sendMessage(payload, [notification.receiver.phoneToken], "${notification.id}")
	    
	    [id: notification.id, message: response.errorCode, status: (response.errorCode ? 'failed' : 'sent')]
    }
}
