package app.tuturno

import grails.transaction.Transactional
import grails.util.Environment

@Transactional
class ITCloudSMSNotificationService {

	static String endpoint = 'https://sistemasmasivos.com'
	static String USERNAME = "jonalvc@gmail.com"
	static String PASSWORD = "b1vOuZCUfJ"
	
	def companyService

	def sendNotification(notification) {
		if (notification.statusCode != 'pending') {
			return [id: notification.id, status: notification.statusCode, message: notification.statusMessage]
		}

		def postParams = [
			user: USERNAME,
			password: PASSWORD,
			GSM: notification.receiver.phoneNumber,
			SMSText: notification.message
		]

		def resp = null

		if (companyService.getPropertyConfig(notification.company, "notifications.sms.mock")?.value == 'true') {
			resp = [data: "57${notification.receiver.phoneNumber}, 1"]
		} else {
			withRest(uri: endpoint) {
				resp = post(path : '/itcloud/api/sendsms/send.php', body: postParams, requestContentType : groovyx.net.http.ContentType.URLENC)
			}
		}

		println notification.message
		
		def respData = resp.data.toString()
		def respSplit = respData.split(', ')
		def respCode
		
		if (respSplit.length == 1) {
			respCode = respSplit[0] as Integer
		} else {
			respCode = respSplit[1] as Integer
		}
		
		[id: notification.id, status: (respCode > 0 ? 'sent' : 'failed'), message: respData]
	}
}
