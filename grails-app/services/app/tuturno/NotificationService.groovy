
package app.tuturno

import grails.converters.JSON
import grails.transaction.Transactional
import groovy.json.JsonSlurper
import groovy.text.SimpleTemplateEngine

@Transactional
class NotificationService {
	
	def companyService

	Notification serializeNotification(notification) {
		notification.save(flush:true, failOnError: true)
		return notification
	}
	
	void notificationResponse(response) {
		def notification = Notification.findById(response.id)
		notification.statusCode = response.status
		notification.statusMessage = response.message
	}

	void acknowledgeNotification(uuid) {
		def notification = Notification.findByUuid(uuid)
		notification?.statusCode = 'received'
	}

	Notification findTurnRegistrationNotificationByTurnId(turn) {
		return Notification.findByTypeLikeAndDataLike('%TurnRegistration', '%"turnId":' + "$turn%")
	}

	Notification decoratePlainTextNotification(notification) {
		notification.attach()
		
		def notificationData = new JsonSlurper().parseText(notification.data)
		
		if (!notification.subject) {
			def plainTextSubjectConfig = companyService.getProperty(notification.company, "notifications.${notification.type}.${notification.receiver.phoneOS}.subject")

			if (!plainTextSubjectConfig) {
				plainTextSubjectConfig = companyService.getProperty(notification.company, "notifications.${notification.type}.subject")
			}
			
			if (plainTextSubjectConfig) {
				try {
					notification.subject = new SimpleTemplateEngine().createTemplate(plainTextSubjectConfig.value).make(notificationData).toString()
				} catch (Exception e) {
					notification.statusCode = 'failed'
					notification.statusMessage = e.message
				}
			} else {
				notification.statusCode = 'failed'
				notification.statusMessage = "No subject plain text configuration for $notification.type notifications for company $notification.company"
			}
		}
		
		if (!notification.message) {
			def plainTextConfig = companyService.getProperty(notification.company, "notifications.${notification.type}.${notification.receiver.phoneOS}.message")

			if (!plainTextConfig) {
				plainTextConfig = companyService.getProperty(notification.company, "notifications.${notification.type}.message")
			}
			
			if (plainTextConfig) {
				try {
					notification.message = new SimpleTemplateEngine().createTemplate(plainTextConfig.value).make(notificationData).toString()
				} catch (Exception e) {
					notification.statusCode = 'failed'
					notification.statusMessage = e.message
				}
			} else {
				notification.statusCode = 'failed'
				notification.statusMessage = "No plain text configuration for $notification.type notifications for company $notification.company"
			}
		}
		
		notificationData['subject'] = notification.subject
		notificationData['message'] = notification.message
		notificationData.remove('out')
		notification.data = (notificationData as JSON).toString()
		
		return notification
	}
}
