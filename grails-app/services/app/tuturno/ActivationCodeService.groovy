package app.tuturno

import grails.converters.JSON
import grails.transaction.Transactional

import org.springframework.integration.support.MessageBuilder

@Transactional
class ActivationCodeService {

	def grailsApplication
	def activationCodeChannel

	def generate(subscriberId) {
		def subscriber = Subscriber.get(subscriberId)

		if (subscriber) {
			subscriber.status = 'inactive'

			def activationCode = ActivationCode.findBySubscriber(subscriber)
			def code = String.format('%04d', new Random().nextInt(10000))

			if (activationCode) {
				activationCode.code = code
			} else {
				activationCode =  new ActivationCode(subscriber: subscriber, code: code)
				activationCode.save(flush: true)
			}

			activationCodeChannel.send(MessageBuilder.withPayload(activationCode).build())
		}

		return subscriber
	}

	def notifyActivationCodeGeneration(activationCode) {
		def notification = new Notification(
				company: grailsApplication.config.tuturnoCompany,
				uuid: UUID.randomUUID().toString(),
				receiver: activationCode.subscriber,
				type: 'ActivationCodeGeneration',
				method: 'sms',
				data: ([activationCode: activationCode.code] as JSON).toString())

		return notification
	}
}
