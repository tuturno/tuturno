package app.tuturno

import es.osoco.android.gcm.*
import grails.async.*
import grails.converters.JSON
import grails.transaction.Transactional

@Transactional
class TurnCallService {

	def androidGcmService
	def companyService
	def sessionFactory

	def nextTurn(queueId) {
		def queue = Queue.findById(queueId)
		def turnCall = null

		if (queue) {
			synchronized(queue) {
				def nextTurnNumber = TurnUtils.calculateNextTurnCall(queue)
				queue.currentTurn = nextTurnNumber

				def turn = NumericTurn.findByQueueAndNumberAndStatus(queue, nextTurnNumber, 'pending', [sort: "dateCreated", order: "asc"])
				turn?.status = 'attended'

				turnCall = new TurnCall(queue: queue, number: nextTurnNumber, turn: turn, jumpElapsedTimeCalculation: (queue.jumpNextElapsedTimeCalculation ?: false)).save(flush: true)

				// Last X days average
				def lastXDays = companyService.getProperty(queue.company, 'stats.average.days')
				
				def fromDatetime = Calendar.instance
				fromDatetime.add(Calendar.DAY_OF_MONTH, -(lastXDays ? lastXDays.value as Integer : 10))

				def turnCalls = TurnCall.findAll("from TurnCall where queue = ? and dateCreated > ? order by dateCreated desc", [queue, fromDatetime.time])
				def timeLapseAverage = TurnUtils.calculateTurnCallsTimeLapseAverage(turnCalls)

				queue.turnCallsTimeLapseAverage = timeLapseAverage > 0 ? timeLapseAverage : queue.turnCallsTimeLapseAverage
				queue.lastTurnCallDatetime = turnCall.dateCreated
				queue.jumpNextElapsedTimeCalculation = (queue.currentTurn == queue.lastAssignedTurn)
			}
		}

		return turnCall
	}

	def notifyTurnCall(turnCall) {
		def turnsToNotify = Turn.executeQuery("select tu from Turn as tu inner join fetch tu.subscriber as su where tu.status = ? and tu.queue = ? and tu.subscriber is not null and su.status = ? and su.activeNotifications = ? and su.phoneOS in ('android', 'ios')", [
			'pending',
			turnCall.queue,
			'active',
			true
		])

		def subscribersToNotify = new HashSet<Subscriber>()
		turnsToNotify.each { turn ->
			subscribersToNotify.add(turn.subscriber)
		}

		if (turnCall.turn?.subscriber) {
			subscribersToNotify.add(turnCall.turn.subscriber)
		}

		def type = 'TurnCall'
		def yourTurnType = 'YourTurnCall'
		def companyConfig = companyService.getActiveConfig(turnCall.queue.company)

		def notificationsList = []
		subscribersToNotify.each { subscriber ->
			def uuid = UUID.randomUUID().toString()
			def data = ([companyConfigId: companyConfig.id,
				companyConfigVersion: companyConfig.version,
				companyName: turnCall.queue.company.name,
				queue: turnCall.queue.id,
				turnCallsTimeLapseAverage: turnCall.queue.turnCallsTimeLapseAverage,
				lastTurnCallDatetime: turnCall.queue.lastTurnCallDatetime,
				uuid: uuid,
				type: subscriber == turnCall.turn?.subscriber ? yourTurnType : type,
				number: turnCall.number] as JSON).toString()

			def notification = new Notification(
					company: turnCall.queue.company,
					uuid: uuid,
					receiver: subscriber,
					type: subscriber == turnCall.turn?.subscriber ? yourTurnType : type,
					data: data)

			if (notification.receiver.phoneOS == 'android' || notification.receiver.phoneOS == 'ios') {
				notification.method = 'push'
			} else if (notification.receiver.phoneOS == 'other') {
				notification.method = 'sms'
			}

			notificationsList << notification
		}

		return notificationsList
	}
}
