databaseChangeLog = {

	changeSet(author: "Jonny Alexander Vele (generated)", id: "1412825341618-1") {
		modifyDataType(columnName: "data", newDataType: "longtext", tableName: "notification")
	}
}
