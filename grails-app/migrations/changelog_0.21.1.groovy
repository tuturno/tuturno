databaseChangeLog = {

	changeSet(author: "Jonny Alexander Vele (generated)", id: "1411881902604-1") {
		addColumn(tableName: "turn") {
			column(name: "status_message", type: "varchar(255)")
		}
	}
}
