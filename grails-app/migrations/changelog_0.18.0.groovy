databaseChangeLog = {

	changeSet(author: "Jonny Alexander Vele (generated)", id: "1410495692108-1") {
		createTable(tableName: "alarm") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "alarmPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "fired", type: "bit") {
				constraints(nullable: "false")
			}

			column(name: "turn_id", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "class", type: "varchar(255)") {
				constraints(nullable: "false")
			}

			column(name: "minutes_left", type: "integer")
		}
	}

	changeSet(author: "Jonny Alexander Vele (generated)", id: "1410495692108-3") {
		createIndex(indexName: "FK5897A51F62ADD84", tableName: "alarm") {
			column(name: "turn_id")
		}
	}

	changeSet(author: "Jonny Alexander Vele (generated)", id: "1410495692108-2") {
		addForeignKeyConstraint(baseColumnNames: "turn_id", baseTableName: "alarm", constraintName: "FK5897A51F62ADD84", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "turn", referencesUniqueColumn: "false")
	}
}
