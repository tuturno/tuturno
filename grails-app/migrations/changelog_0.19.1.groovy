databaseChangeLog = {

	changeSet(author: "Jonny Alexander Vele (generated)", id: "1410709500097-1") {
		addColumn(tableName: "config_property") {
			column(name: "name", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "Jonny Alexander Vele (generated)", id: "1410709500097-2") {
		dropColumn(columnName: "key", tableName: "config_property")
	}
}
