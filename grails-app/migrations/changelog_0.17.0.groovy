databaseChangeLog = {

	changeSet(author: "Jonny Alexander Vele (generated)", id: "1410404382859-1") {
		addColumn(tableName: "notification") {
			column(name: "company_id", type: "bigint")
		}
	}

	changeSet(author: "Jonny Alexander Vele (generated)", id: "1410404382859-2") {
		addColumn(tableName: "queue") {
			column(name: "last_assigned_turn", type: "integer")
		}
	}

	changeSet(author: "Jonny Alexander Vele (generated)", id: "1410404382859-3") {
		addColumn(tableName: "queue") {
			column(name: "turn_calls_time_lapse_average", type: "integer")
		}
	}

	changeSet(author: "Jonny Alexander Vele (generated)", id: "1410404382859-4") {
		addColumn(tableName: "turn_call") {
			column(name: "number", type: "integer") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "Jonny Alexander Vele (generated)", id: "1410404382859-5") {
		modifyDataType(columnName: "turn_id", newDataType: "bigint", tableName: "turn_call")
	}

	changeSet(author: "Jonny Alexander Vele (generated)", id: "1410404382859-6") {
		dropNotNullConstraint(columnDataType: "bigint", columnName: "turn_id", tableName: "turn_call")
	}

	changeSet(author: "Jonny Alexander Vele (generated)", id: "1410404382859-9") {
		createIndex(indexName: "FK237A88EBEC238B70", tableName: "notification") {
			column(name: "company_id")
		}
	}

	changeSet(author: "Jonny Alexander Vele (generated)", id: "1410404382859-10") {
		createIndex(indexName: "FKF89078E0F62ADD84", tableName: "turn_call") {
			column(name: "turn_id")
		}
	}

	changeSet(author: "Jonny Alexander Vele (generated)", id: "1410404382859-7") {
		addForeignKeyConstraint(baseColumnNames: "company_id", baseTableName: "notification", constraintName: "FK237A88EBEC238B70", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "company", referencesUniqueColumn: "false")
	}

	changeSet(author: "Jonny Alexander Vele (generated)", id: "1410404382859-8") {
		addForeignKeyConstraint(baseColumnNames: "turn_id", baseTableName: "turn_call", constraintName: "FKF89078E0F62ADD84", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "turn", referencesUniqueColumn: "false")
	}
}
