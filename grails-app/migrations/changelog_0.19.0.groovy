databaseChangeLog = {

	changeSet(author: "Jonny Alexander Vele (generated)", id: "1410664627095-1") {
		addColumn(tableName: "alarm") {
			column(name: "enabled", type: "bit") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "Jonny Alexander Vele (generated)", id: "1410664627095-2") {
		addColumn(tableName: "config_property") {
			column(name: "public_visible", type: "bit") {
				constraints(nullable: "false")
			}
		}
	}
}
