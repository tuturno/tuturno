databaseChangeLog = {

	changeSet(author: "Jonny Alexander Vele (generated)", id: "1411874727463-1") {
		addColumn(tableName: "queue") {
			column(name: "jump_next_elapsed_time_calculation", type: "bit")
		}
	}

	changeSet(author: "Jonny Alexander Vele (generated)", id: "1411874727463-2") {
		addColumn(tableName: "turn_call") {
			column(name: "jump_elapsed_time_calculation", type: "bit")
		}
	}
}
