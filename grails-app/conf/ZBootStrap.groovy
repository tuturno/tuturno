import app.tuturno.*
import grails.util.Environment

class ZBootStrap {
	
	def grailsApplication
	
    def init = { servletContext ->
		// loads tuturno company on grailsApplication
		def tuturnoCompany = Company.findByCode('tuturno')
		
		if (!tuturnoCompany) {
			tuturnoCompany = new Company(code: 'tuturno', name: 'TuTurno', user: User.findByUsername("admin")).save(failOnError: true)
		}
		
		grailsApplication.config.tuturnoCompany = tuturnoCompany
		
    	Environment.executeForCurrentEnvironment {
      		development {
				def tuturnoConfig = new CompanyConfig(company: tuturnoCompany, name: 'default configuration', active: true).save(failOnError: true)
				new ConfigProperty(companyConfig: tuturnoConfig, name: 'notifications.sms.mock', value: 'true').save(failOnError: true)
				new ConfigProperty(companyConfig: tuturnoConfig, name: 'notifications.ActivationCodeGeneration.subject', value: '$companyName').save(failOnError: true)
				new ConfigProperty(companyConfig: tuturnoConfig, name: 'notifications.ActivationCodeGeneration.message', value: 'Codigo de activacion TuTurno: $activationCode').save(failOnError: true)
				new ConfigProperty(companyConfig: tuturnoConfig, name: 'notifications.WelcomeSubscriber.subject', value: 'TuTurno').save(failOnError: true)
				new ConfigProperty(companyConfig: tuturnoConfig, name: 'notifications.WelcomeSubscriber.message', value: 'Bienvenido, lo invitamos a instalar la aplicacion TuTurno en tu smartphone http://tuturnoapp.com').save(failOnError: true)
				
				def user = User.findByUsername("user")
        		def company = new Company(code: 'test_company', name: 'Test Company', user: user).save(failOnError: true)
				def config = new CompanyConfig(company: company, name: 'test configuration', active: true).save(failOnError: true)
				new ConfigProperty(companyConfig: config, name: 'notifications.NumericTurnRegistration.subject', value: '$companyName').save(failOnError: true)
				new ConfigProperty(companyConfig: config, name: 'notifications.NumericTurnRegistration.message', value: 'Su turno en $companyName es $number').save(failOnError: true)
				new ConfigProperty(companyConfig: config, name: 'notifications.YourTurnCall.subject', value: '$companyName').save(failOnError: true)
				new ConfigProperty(companyConfig: config, name: 'notifications.YourTurnCall.message', value: 'Es su turno en $companyName').save(failOnError: true)
				new ConfigProperty(companyConfig: config, name: 'notifications.TimeAlarm.subject', value: '$companyName').save(failOnError: true)
				new ConfigProperty(companyConfig: config, name: 'notifications.TimeAlarm.message', value: 'Falta aproximadamente $minutesLeft min para su turno en $companyName').save(failOnError: true)
				new ConfigProperty(companyConfig: config, name: 'notifications.TurnCancellation.subject', value: '$companyName').save(failOnError: true)
				new ConfigProperty(companyConfig: config, name: 'notifications.TurnCancellation.message', value: 'Su turno en $companyName ha sido cancelado. $reason').save(failOnError: true)
				new ConfigProperty(companyConfig: config, name: 'notifications.WelcomeSubscriber.disabled', value: 'true').save(failOnError: true)
				
        		def queue = new Queue(company: company, code: 'testqueue', name: 'Test Queue', prefix: 'TQ').save(failOnError: true)
        		def numericQueue = new NumericQueue(company: company, code: 'numqueue', name: 'NumQueue', prefix: 'NQ', maxTurn: 99).save(failOnError: true)
      		}
      	}
    }
	
    def destroy = {
    }
}
