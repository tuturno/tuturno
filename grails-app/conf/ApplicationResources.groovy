modules = {
    application {
        resource url:'js/application.js'
    }
	
	overrides {
        'bootstrap-css' {
            defaultBundle configDefaultBundle
            resource id: 'bootstrap-css', url:'css/bootstrap.css'
        }
		
		'nimble-login-css' {
			resource id:'css', url:'/css/nimble-login.css'
		}
    } 
}