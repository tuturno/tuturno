import grails.converters.JSON

class BootStrap {
	
    def init = { servletContext ->
		JSON.registerObjectMarshaller(Date) {
			return it?.time
		 }
    }
	
    def destroy = {
    }
}
