package app.tuturno

import org.joda.time.DateTime

class TurnController {
	
	def notificationService

	def listBySubscriberAndStatus(Long subscriber, String status, Integer max) {
		params.max = Math.min(max ?: 10, 100)

		def query = 'from Turn where subscriber.id = :subscriber and status = :status'
		def queryParams = [subscriber: subscriber, status: status]
		
        respond Turn.executeQuery(query, queryParams, params)
    }

	def listByQueueAndDatetimeRange(Long queue, String from, String thru) {
		def fromDate, thruDate
		
		try {
			fromDate = DateTime.parse(from)
		} catch (ex) {
			getErrors().rejectValue('from', 'exception.message', ex.message)
		}
		
		try {
			thruDate = DateTime.parse(thru)
		} catch (ex) {
			getErrors().rejectValue('thru', 'exception.message', ex.message)
		}
		
		if (getErrors().errorCount) {
			respond getErrors()
			return
		}
		
		def query = 'select t, sub.phoneNumber from Turn as t inner join fetch t.subscriber as sub ' +
					'where t.queue.id = :queue ' +
					'and t.datetime between :from and :thru'		
		def queryParams = [queue: queue, from: fromDate.toDate(), thru: thruDate.toDate()]
		
        respond Turn.executeQuery(query, queryParams), view: 'index'
    }
	
	def getTurnRegistrationNotification(Long turn) {
		respond notificationService.findTurnRegistrationNotificationByTurnId(turn)
	}
}
