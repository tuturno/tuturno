package app.tuturno

class SubscriberController {
    static scaffold = true
	
	def subscriberService
	
	def saveOrUpdate(Subscriber subscriber) {
		subscriber = subscriberService.saveOrUpdate(subscriber)
		
		if (subscriber.hasErrors()) {
			respond subscriber.errors
		} else {
			respond subscriber
		}
	}
	
	def activate(ActivationCode activationCode) {
		def generatedActivationCode = ActivationCode.findBySubscriber(activationCode.subscriber)
		
		if (activationCode.code == generatedActivationCode?.code) {
			activationCode.subscriber.status = 'active'
		}
		
		respond activationCode.subscriber
	}
	
	def activateNotifications() {
		def subscriber = Subscriber.findById(request.JSON.subscriber)
		
		if (!subscriber) {
			def res = [status: 'error', message: 'subscriber not found']
			respond res
		} else {
			subscriber.activeNotifications = true
			respond subscriber
		}
	}
	
	def deactivateNotifications() {
		def subscriber = Subscriber.findById(request.JSON.subscriber)
		
		if (!subscriber) {
			def res = [status: 'error', message: 'subscriber not found']
			respond res
		} else {
			subscriber.activeNotifications = false
			respond subscriber
		}
	}
}
