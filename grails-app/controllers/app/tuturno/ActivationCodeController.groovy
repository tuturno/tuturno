package app.tuturno

import org.springframework.integration.support.MessageBuilder

class ActivationCodeController {
	static scaffold = true
	
	def activationCodeService

	def generate() {
		def subscriber = activationCodeService.generate(request.JSON.subscriber)
		
		if (subscriber) {
			respond subscriber
		}
		
		def res = [:]
		res.status = 'error'
		res.message = 'subscriber not found'
		
		respond res 
	}
}
