package app.tuturno

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import org.springframework.integration.support.MessageBuilder

class NumericTurnController {
	static scaffold = true

	def turnRegistrationChannel
	def turnsService
	def subscriberService

	@Transactional
	def newTurn() {
		request.withFormat {
			json {
				NumericTurn numericTurnInstance = new NumericTurn(request.JSON)
				numericTurnInstance.number = -1

				if (request.JSON.number) {
					numericTurnInstance.number = request.JSON.number
				}
				
				if (request.JSON.phoneNumber) {
					numericTurnInstance.subscriber = Subscriber.findByPhoneNumber(request.JSON.phoneNumber)

					if (!numericTurnInstance.subscriber) {
						numericTurnInstance.subscriber = subscriberService.saveOrUpdate(new Subscriber(phoneNumber: request.JSON.phoneNumber), request.JSON.queue)
					}
				}

				numericTurnInstance.validate()
				if (numericTurnInstance.hasErrors()) {
					respond numericTurnInstance.errors
					return
				}
				
				if (request.JSON.number) {
					turnsService.storeNumericTurn(numericTurnInstance)
				} else {
					turnsService.generateNumericTurn(numericTurnInstance)
				}

				if (numericTurnInstance.subscriber) {
					turnRegistrationChannel.send(MessageBuilder.withPayload(numericTurnInstance).build())
				}

				respond numericTurnInstance, [status: CREATED]
			}
			'*'{ notFound() }
		}
	}
}
