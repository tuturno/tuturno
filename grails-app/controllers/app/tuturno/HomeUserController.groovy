package app.tuturno

import org.apache.shiro.SecurityUtils

class HomeUserController {

    def index() {

    	if( session.getAttribute('app.tuturno.queueType')==null ) {
    		def loggedUser = User.get(SecurityUtils.getSubject()?.getPrincipal())
			if( loggedUser?.company?.queues) {

	            def currentCompany = loggedUser.company
	            def q = currentCompany.queues[0];
	            if(q instanceof app.tuturno.NumericQueue) {
	                //redirect(controller: "HomeUserNQ");
	                println "NQ"
	                session.setAttribute('app.tuturno.queueType', 'NQ')
	            } else {
	                //redirect(controller: "HomeUserFQ");
	                println "FQ"
	                session.setAttribute('app.tuturno.queueType', 'FQ')
	            }
	        }
    	}        
        
    }
}
