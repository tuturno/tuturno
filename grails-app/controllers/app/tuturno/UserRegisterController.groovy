package app.tuturno

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import org.apache.shiro.SecurityUtils
import grails.plugin.nimble.core.UserService
import grails.plugin.nimble.core.UserBase
import grails.plugin.nimble.core.ProfileBase
import grails.plugin.nimble.InstanceGenerator

/*
 * Register from API Dashboard
 */
class UserRegisterController {
	
	def userService
	def developerService
	
	@Transactional
	def createAccount() {

		getNimbleConfig();

		request.withFormat {
			json {
				//def user = User.newInstance()
				UserBase user = new UserBase(request.JSON)
				user.profile = ProfileBase.newInstance()
				user.profile.owner = user
				user.properties['username', 'pass', 'passConfirm'] = request.JSON
				user.profile.properties['fullName', 'email'] = request.JSON
				user.enabled = nimbleConfig.localusers.provision.active
				user.external = true

				user.validate()

				log.debug("Attempting to create new user account identified as $user.username")

				// Enforce username restrictions on local accounts, letters + numbers only
				if (user.username == null || user.username.length() < nimbleConfig.localusers.usernames.minlength || !user.username.matches(nimbleConfig.localusers.usernames.validregex)) {
					log.debug("Supplied username of $user.username does not meet requirements for local account usernames")
					user.errors.rejectValue('username', 'nimble.user.username.invalid')
				}

				// Enforce email address for account registrations
				if (user.profile.email == null || user.profile.email.length() == 0) {
					user.profile.email = 'invalid'
				}

				if (user.hasErrors()) {
					log.debug("Submitted values for new user are invalid")
					user.errors.each { log.debug it }

					respond user
				}

				//user.save()
				def savedUser = userService.createUser(user)
				if (savedUser.hasErrors()) {
					log.debug("UserService returned invalid account details when attempting account creation")
					
					respond user
				} else {
					developerService.add(savedUser)
				
					log.info("Sending account registration confirmation email to $user.profile.email with subject $nimbleConfig.messaging.registration.subject")

					if(nimbleConfig.messaging.enabled) {
						sendMail {
							to user.profile.email
							from nimbleConfig.messaging.mail.from
							subject nimbleConfig.messaging.registration.subject
							html g.render(template: "/templates/nimble/mail/accountregistration_email", model: [user: savedUser]).toString()
						}
					}
					else {
						log.debug "Messaging disabled would have sent: \n${user.profile.email} \n Message: \n ${g.render(template: "/templates/nimble/mail/accountregistration_email", model: [user: user]).toString()}"
					}

					log.info("Created new account identified as $user.username with internal id $savedUser.id")

					respond user
				}
				
			}
			'*'{ notFound() }
		}
	}


	private getNimbleConfig() {
		grailsApplication.config.nimble
	}
}
