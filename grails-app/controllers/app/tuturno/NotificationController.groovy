package app.tuturno

import grails.transaction.Transactional

import org.springframework.integration.support.MessageBuilder

class NotificationController {
    static scaffold = true
	
	def acknowledgeNotificationChannel

    @Transactional
    def acknowledge() {
    	def response = [:]

    	if (request.JSON.uuid) {
    		acknowledgeNotificationChannel.send(MessageBuilder.withPayload(request.JSON.uuid).build())
    		response.status = 'SUCCESS'
    	} else {
    		response.status = 'VALIDATION'
    		response.message = 'uuid required'
    	}

    	respond response
    }
}
