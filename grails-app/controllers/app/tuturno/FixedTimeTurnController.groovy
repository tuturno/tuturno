package app.tuturno

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import org.joda.time.DateTime
import org.springframework.integration.support.MessageBuilder

class FixedTimeTurnController {
    static scaffold = true

    def turnRegistrationChannel

    @Transactional
    def createForPhoneNumber() {
        request.withFormat {
            json {
				try {
					request.JSON.datetime = DateTime.parse(request.JSON.datetime).toDate()
				} catch (ex) {
					request.JSON.remove('datetime')
				}
				
                FixedTimeTurn fixedTimeTurnInstance = new FixedTimeTurn(request.JSON)

                if (request.JSON.phoneNumber) {
                    fixedTimeTurnInstance.subscriber = Subscriber.findByPhoneNumber(request.JSON.phoneNumber)

                    if (!fixedTimeTurnInstance.subscriber) {
                        numericTurnInstance.subscriber = subscriberService.saveOrUpdate(new Subscriber(phoneNumber: request.JSON.phoneNumber), request.JSON.queue)
                    }

                    fixedTimeTurnInstance.validate()
                }

                if (fixedTimeTurnInstance.hasErrors()) {
                    respond fixedTimeTurnInstance.errors
                    return
                }

                fixedTimeTurnInstance.save(flush: true)

                turnRegistrationChannel.send(MessageBuilder.withPayload(fixedTimeTurnInstance).build())

                respond fixedTimeTurnInstance, [status: CREATED]
            }
            '*'{ notFound() }
        }
    }
}
