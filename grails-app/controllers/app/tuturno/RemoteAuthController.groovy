package app.tuturno

import org.apache.shiro.SecurityUtils
import org.apache.shiro.authc.AuthenticationException
import org.apache.shiro.authc.DisabledAccountException
import org.apache.shiro.authc.IncorrectCredentialsException
import org.apache.shiro.authc.UsernamePasswordToken

class RemoteAuthController {

	def userService

    def signin(String username, String password) {

    	def response = [:]

    	def authToken = new UsernamePasswordToken(username, password)

    	try {
    		SecurityUtils.subject.login(authToken)

    		def loggedUser = User.findByUsername(username)

    		if(loggedUser==null) {
    			response.status = 500
				response.message = message(code: 'login.credentials.notexist') //"Username not found"
    		} else if(loggedUser?.remoteapi) {
    			response.status = 200
    			response.user = [id:loggedUser.id, username:loggedUser.username, company:loggedUser.company]
			} else {
				response.status = 500
				response.message = message(code: 'login.remoteapi.notallowed') //"Remote login is not allowed"
			}

		} catch(IncorrectCredentialsException ex) {
			response.status = 500
			response.message = message(code:'login.credentials.failure', args:[username]) //"Credentials failure for user '${username}'"
		} catch (AuthenticationException ex) {
			response.status = 500
			response.message = message(code:'login.authentication.failure', args:[username]) //"General authentication failure for user '${username}'."
		}

		respond response
    }

    def signout( String username ) {
    	
    	def userTemp = User.findByUsername( username )
    	//userTemp.logout
    	SecurityUtils.subject?.logout()

    	def response = ['status':'OK']

    	respond response

    	//SecurityUtils.subject?.logout()
    }

    def localSignout() {

        log.info("Signing out user ${authenticatedUser?.username}")

        if(userService.events["logout"]) {
            log.info("Executing logout callback")
            userService.events["logout"](authenticatedUser)
        }

        session.invalidate();

        SecurityUtils.subject?.logout()
        redirect(uri: '/')
    }
}
