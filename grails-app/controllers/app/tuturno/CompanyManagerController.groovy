package app.tuturno

import static org.springframework.http.HttpStatus.*
import org.apache.shiro.SecurityUtils
import grails.plugin.nimble.core.UserBase

class CompanyManagerController {

	def companyService

    def index() {
		def loggedUser = User.get(SecurityUtils.getSubject()?.getPrincipal())
		
		def currentCompany = loggedUser.company;
		
		//Por ahora todos van a manejar solo 1 cola, la primera creada para la actual Company
		def userData = [user_queue_id:currentCompany.queues[0]?.id, user_company_id:currentCompany.id]

		if( session.getAttribute('app.tuturno.queueType')=='NQ' ) {
			render(view:"indexNQ", model:userData);
		} else if( session.getAttribute('app.tuturno.queueType')=='FQ' ) {
			render(view:"indexFQ", model:userData);
		}
	}
	
	def getConfiguration(Long company) {
		def companyEntity = Company.get(company)
		def config = companyEntity ? companyService.getActiveConfig(companyEntity) : null
		
		if (!companyEntity || !config) {
			render status: NOT_FOUND
			return
		}
		
		def properties = [:] << config.properties
		properties.id = config.id
		properties.version = config.version
		properties.remove('configProperties')
		properties.remove('company')
		properties.remove('active')
		config.configProperties.each {
			if (it.publicVisible) {
				properties[it.name] = it.value
			}
		}
		
		respond properties
	}
}
