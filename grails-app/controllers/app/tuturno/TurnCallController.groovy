package app.tuturno

import grails.transaction.Transactional

import org.springframework.integration.support.MessageBuilder

class TurnCallController {
    static scaffold = true

    def turnCallService
	def turnCallChannel
	
	@Transactional
    def nextTurn() {
		request.withFormat {
			json {
				def turnCall = turnCallService.nextTurn(request.JSON.queue)
				
				if (turnCall) {
					turnCallChannel.send(MessageBuilder.withPayload(turnCall).build())
					
					def res = [turnCall: turnCall, queue: turnCall.queue]
					respond res
				} else {
					def res = [status: 'no_more_turns']
					respond res
				}
			}
			'*'{ notFound() }
		}
    }
}
