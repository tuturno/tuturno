package app.tuturno

class CompanyController {
    static scaffold = true
	
	def companyService
    
    def panelUser() {
    	
    }
	
	def getByCodeWithQueues(String code) {
		def company = Company.findByCode(code)
		if(company) {
			def data = [company: company, queues: company.queues]
		
			respond data
		} else {
			def response = [:]

			response.status = 'ERROR'
    		response.message = 'Company not found'

    		respond response
		}
	}
	
	def getPublicConfigProperties(String code) {
		def company = Company.findByCode(code)
		if(company) {
			respond companyService.getPublicVisibleProperties(company)
		} else {
			def res = [:]

			res.status = 'ERROR'
			res.message = 'Company not found'

			respond res
		}
	}
}
