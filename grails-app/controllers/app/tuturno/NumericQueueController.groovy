package app.tuturno

class NumericQueueController {
	def scaffold = true
	
	def numericQueueService
	
	def reset(NumericQueue queue) {
		if (queue) {
			numericQueueService.reset(queue)
			respond queue
		} else {
			def res = [status: 'error', message: 'numeric queue not found']
			respond res
		}
	}
}
