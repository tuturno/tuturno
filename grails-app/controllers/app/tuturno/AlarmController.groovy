package app.tuturno

class AlarmController {

    def listByTurn(Integer turn) {
		respond Alarm.findAllByTurn(Turn.get(turn))
	}
	
	def enable() {
		def alarm = Alarm.get(request.JSON.alarm)
		def res = [status: 'ok']
		
		if (alarm) {
			alarm.enabled = true
		} else {
			res.status = 'not_found'
		}
		
		respond res
	}
	
	def disable() {
		def alarm = Alarm.get(request.JSON.alarm)
		def res = [status: 'ok']
		
		if (alarm) {
			alarm.enabled = false
		} else {
			res.status = 'not_found'
		}
		
		respond res
	}
}
