<ul class="nav navbar-nav side-nav">

    <li class="${controllerName == 'main' ? 'active' : ''}">
      <g:link controller="main" action="index">
         <i class="fa fa-dashboard"></i>
         <span>Dashboard</span>                     
      </g:link>
      <i class="icon-chevron-right"></i>
    </li>
    
    <li class="${controllerName == 'subscriber' ? 'active' : ''}">
      <g:link controller="subscriber" action="index">
         <i class="fa fa-mobile-phone"></i>
         <span>Subscribers</span>                     
      </g:link>
      <i class="icon-chevron-right"></i>
    </li>
    
    <li class="${controllerName == 'activationCode' ? 'active' : ''}">
      <g:link controller="activationCode" action="index">
         <i class="fa fa-mobile-phone"></i>
         <span>Activation Codes</span>                     
      </g:link>
      <i class="icon-chevron-right"></i>
    </li>
    
    <li class="${controllerName == 'notification' ? 'active' : ''}">
      <g:link controller="Notification" action="index">
         <i class="fa fa-mobile"></i>
         <span>Notifications</span>                     
      </g:link>
      <i class="icon-chevron-right"></i>
    </li>
    
    <!--==== Company Management ====-->
    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
      <i class="fa fa-caret-square-o-down"></i> Companies <b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li class="${controllerName == 'company' ? 'active' : ''}">
          <g:link controller="company" action="index">
             <i class="fa fa-building-o"></i>
             <span>Companies</span>                     
          </g:link>
          <i class="icon-chevron-right"></i>
        </li>
        <li class="${controllerName == 'companyConfig' ? 'active' : ''}">
          <g:link controller="companyConfig" action="index">
             <i class="fa fa-building-o"></i>
             <span>Companies Config</span>                     
          </g:link>
          <i class="icon-chevron-right"></i>
        </li>
        <li class="${controllerName == 'configProperty' ? 'active' : ''}">
          <g:link controller="configProperty" action="index">
             <i class="fa fa-building-o"></i>
             <span>Config Properties</span>                     
          </g:link>
          <i class="icon-chevron-right"></i>
        </li>

      </ul>
    </li>

    <!--==== Queues Management ====-->
    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
      <i class="fa fa-caret-square-o-down"></i> Queues <b class="caret"></b></a>
      <ul class="dropdown-menu">

        <li class="${controllerName == 'queue' ? 'active' : ''}">
          <g:link controller="Queue" action="index">
             <i class="fa fa-angle-double-right"></i>
             <span>Queues</span>                     
          </g:link>
          <i class="icon-chevron-right"></i>
        </li>
        <li class="${controllerName == 'numericQueue' ? 'active' : ''}">
          <g:link controller="NumericQueue" action="index">
             <i class="fa fa-angle-double-right"></i>
             <span>Numeric Queues</span>                     
          </g:link>
          <i class="icon-chevron-right"></i>
        </li>

      </ul>
    </li>

    <!--==== Turns Management ====-->
    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
      <i class="fa fa-caret-square-o-down"></i> Turns <b class="caret"></b></a>
      <ul class="dropdown-menu">

        <li class="${controllerName == 'numericTurn' ? 'active' : ''}">
          <g:link controller="NumericTurn" action="index">
             <i class="fa fa-sort-numeric-asc"></i>
             <span>Numeric Turns</span>                     
          </g:link>
          <i class="icon-chevron-right"></i>
        </li>
        <li class="${controllerName == 'fixedTimeTurn' ? 'active' : ''}">
          <g:link controller="FixedTimeTurn" action="index">
             <i class="fa fa-clock-o"></i>
             <span>Fixed Time Turns</span>                     
          </g:link>
          <i class="icon-chevron-right"></i>
        </li>
        <li class="${controllerName == 'turnCall' ? 'active' : ''}">
          <g:link controller="TurnCall" action="index">
             <i class="fa fa-flash"></i>
             <span>Turn Calls</span>                     
          </g:link>
          <i class="icon-chevron-right"></i>
        </li>
        <li class="${controllerName == 'timeAlarm' ? 'active' : ''}">
          <g:link controller="TimeAlarm" action="index">
             <i class="fa fa-clock-o"></i>
             <span>Time Alarms</span>                     
          </g:link>
          <i class="icon-chevron-right"></i>
        </li>

      </ul>
    </li>


        

    <li class="dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
      <i class="fa fa-caret-square-o-down"></i> User <b class="caret"></b></a>
      <ul class="dropdown-menu">
        <!-- <li class="${controllerName == 'admins' ? 'active' : ''}">
          <g:link controller="admins" action="index">
             <i class="fa fa-users"></i>
             <span><g:message code="nimble.link.admins" /></span>                                    
          </g:link>
          <i class="icon-chevron-right"></i> 
        </li> -->
        <li class="${controllerName == 'user' ? 'active' : ''}">       
          <g:link controller="user" action="list">
             <i class="fa fa-users"></i>
             <span><g:message code="nimble.link.users" /></span>
          </g:link>
          <i class="icon-chevron-right"></i>
        </li>
        <li class="${controllerName == 'role' ? 'active' : ''}">
          <g:link controller="role" action="list">
             <i class="fa fa-user-md"></i>
             <span><g:message code="nimble.link.roles" /></span>      
          </g:link>
          <i class="icon-chevron-right"></i>
        </li>
        <li class="${controllerName == 'group' ? 'active' : ''}">
          <g:link controller="group" action="list">
             <i class="fa fa-group"></i>
             <span><g:message code="nimble.link.groups" /></span>
          </g:link>
          <i class="icon-chevron-right"></i>
        </li>
      </ul>
    </li>

</ul>