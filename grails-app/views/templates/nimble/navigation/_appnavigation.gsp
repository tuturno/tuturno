<ul class="nav navbar-nav side-nav">
  	<li class="${controllerName == 'homeUser' ? 'active-menu-item' : ''}">
	  <g:link controller="homeUser" action="index">
	    <i class="fa fa-dashboard"></i>
	    <span> <g:message code="menu.dashboard"/></span>                   
	  </g:link>
	  <i class="icon-chevron-right"></i>
	</li>
	<li class="${controllerName == 'companyManager' ? 'active-menu-item' : ''}">
	  <g:link controller="companyManager" action="index">
	    <i class="fa fa-dashboard"></i>
	    <span> <g:message code="default.turn.management"/></span>
	  </g:link>
	  <i class="icon-chevron-right"></i>
	</li>
</ul>