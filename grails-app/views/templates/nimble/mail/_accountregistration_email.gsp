<html>

<head>

</head>

<body>
	
	<h2>
		Your Neverlines account is ready!
	</h2>

	<p>
  		<!-- <a href="${createLink(absolute:true, controller: 'account', action: 'validateuser', id: user.id, params: [activation: user.actionHash])}"><g:message code="nimble.link.activateaccount" /></a> -->

  		You just signed up for the Neverlnes API, an online service that<br/>
  		lets you manage Turns in Queues or Waiting lines with the powerful<br/>
  		of the push/sms notifications and automatize the turns control.<br/>
  		Before you get started, please familiarize yourself with the following links<br/>
  		and resources in case you have any questions or problems.
	</p>

	<p>API Access: <br/>
	   &nbsp;Login URL: http://dashboard.tuturnoapp.com/login<br/>
	   &nbsp;Username: ${user.username}<br/>
	   &nbsp;API key: ................
	</p>

	<p>Support: <br/>
	   &nbsp;Email: support@neverlines.com<br/>
	   &nbsp;Forums: http://dashboard.tuturnoapp.com/forums/<br/>
	   &nbsp;FAQ: http://dashboard.tuturnoapp.com/faq/<br/>
	   &nbsp;Twitter: http://twitter.com/neverlines
	</p>

	<p>&nbsp;</p>

	<p>Thank you for subscribe to Neverlines API</p>

	<p>&nbsp;</p>
	<p>--</p>
	<p>The Neverlines API Team</p>

	<!-- <p>
  		<g:message code="nimble.template.mail.accountregistration.trouble" />
	</p>
	<p>
		http://neverlines.com/activate/${user.id}/${user.actionHash}
	</p> -->



</body>

</html>