<!doctype html>
<html>
  <head>
      <title>
         <g:message code="nimble.template.login.title" />
      </title>	  
      <r:require modules="nimble-login"/>
	  <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:300,400,700" type="text/css">
      <r:layoutResources/>
  </head>
  <body class="login">
	<div id="main-welcome" class="row-fluid">
		<!-- First Col -->
		<div id="left-col" class="span6 visible-desktop">
			<img alt="UsuarioApp" src="http://tuturnoapp.com/img/EmpresaGrap.png" class="box-image"/>
			<div class="text">
				<h2>Noticias</h2>
				<p>Aun estamos en fase Beta!! deja tus comentarios, sugerencias y solicitudes en nuestra <a href="http://soporte.tuturnoapp.com">Página de Soporte.</a></p>
			</div>
		</div>
		<!-- end first col-->
		
		<!-- Second Col -->
		<div id="right-col" class="span5">
			<div class="login-container">
				<div class="login-content">
					<img alt="Logo TuTurno" src="http://www.tuturnoapp.com/img/logoAppTuTurno.png" id="logo" />
					<h4 class="title border-bottom"><g:message code="nimble.label.login.signin" /></h4>
					<n:flashembed/>  
					
					
					<g:form action="signin" name="login-form" method="post">
					   <div class="login-input">
						  <div class="control-group">
							 <div class="controls ">
								<% if(!targetUri) { targetUri = "administration/index" } %>
								<input type="hidden" name="targetUri" value="${targetUri}"/>
								<input type="text" name="username" id="username" placeholder="usuario@ejemplo.com">                           
							 </div>
						  </div>
						  <div class="control-group">
							 <div class="controls">
								<input type="password" name="password" id="password" placeholder="contraseña">
							 </div>
						  </div>
					   </div>
					   <div class="login-actions">
						  <label class="checkbox" style="display: inline-block;">
							 <input type="checkbox" name="rememberme">
							 <g:message code="nimble.label.rememberme" />
						  </label>
						  <span class="pull-right clearfix">
							 <button type="submit" class="btn btn-primary">
								<g:message code="nimble.link.login.basic" />
							 </button>
						  </span>
					   </div>
					   <div class="login-options border-top">
						  <h4>
							 <g:message code="nimble.label.login.forgottenpassword.heading" />
						  </h4>                  
						  <g:message code="nimble.label.login.forgottenpassword.descriptive" />
						  <g:link controller="account" action="forgottenpassword" style="text-transform:lowercase;">
							 <g:message code="nimble.link.resetpassword" />
						  </g:link>
					   </div>
					</g:form>
				 </div>
			</div>		
		</div>
		<!-- end second col-->
	</div>
    <r:layoutResources/>
  </body>
</html>