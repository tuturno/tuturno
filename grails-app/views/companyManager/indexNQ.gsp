<%@page import="org.apache.shiro.SecurityUtils"%>
<%@page import="grails.plugin.nimble.core.UserBase"%>
<!DOCTYPE html>
<html>
  <head>
    <meta name="layout" content="app" />
    <title>TuTurno | <g:message code="default.turn.management"/></title>
	<link rel="stylesheet" href="${resource(dir: 'css', file: 'datepicker.css')}" type="text/css">
	<link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap-timepicker.min.css')}" type="text/css">
	<link rel="stylesheet" href="${resource(dir: 'css', file: 'dataTables.bootstrap.css')}" type="text/css">
	<link rel="stylesheet" href="//cdn.datatables.net/tabletools/2.2.1/css/dataTables.tableTools.css" type="text/css">
  </head>
  <body class="companyManager">
    <g:set var="currentUser" value="${UserBase.get(SecurityUtils.subject.principal)}" />
    <div class="row">
        <div id="main-content" class="col-lg-12">
            <h1><g:message code="default.turn.management"/> - ${currentUser.company}</h1>
			<div class="row">
				<div class="col-lg-4 col-md-4"><!-- Start table 1st column -->
					<div class="panel panel-primary">
					  <div class="panel-heading"><g:message code="turn.management.assignturn"/></div>
					  <div class="panel-body">
						<form role="form">
							<div class="form-group">
								<label class="control-label"><g:message code="turn.management.phone.number"/></label>
								<input type="text" class="form-control" id="subscriber" name="subscriber">
							</div>
							<button id="turn-submit" type="button" class="btn btn-primary"><g:message code="turn.management.assignturn"/></button>
							<span id="ajax-loader"></span>
						</form>
					  </div>
					</div>
				</div><!-- End table 1st column -->
				
				<div class="col-lg-8 col-md-8"><!-- Start table 2d column -->
					<div id="turns-panel" class="panel panel-primary">
						<div class="panel-heading"><g:message code="turn.management.assignedturns"/></div>
						<div class="panel-body ">
							<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="dataTable-turnos">
							<thead>
							  <tr>
								<th># <i class="fa fa-sort"></i></th>
								<th>Turno No.</th>
								<th><g:message code="turn.management.phone.number"/></th>
								<th><g:message code="turn.management.date.assignedDate"/></th>
								<th><g:message code="turn.management.date.assignedTime"/></th>
								<th title="Estado de turno"><g:message code="turn.management.status"/></th>
								<th title="Estado de Notificación al Celular"><g:message code="turn.management.notify"/></th>
							  </tr>
							</thead>
							</table>
							</div>
							<hr />
						</div>
						

					</div>
				
				</div><!-- end table 2d column -->
			</div>        </div>
    </div><!-- end-row -->
	
	<script src="http://cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
	<script src="http://cdn.datatables.net/tabletools/2.2.1/js/dataTables.tableTools.min.js"></script>
	<g:javascript src="dataTables/dataTables.bootstrap.js"></g:javascript>
	<g:javascript src="i18n/jquery.i18n.min.js"></g:javascript>
	<g:javascript src="moment.min.js"></g:javascript>
	
	<script type="text/javascript">	
	var saveURL = "${createLink(controller:'fixedTimeTurn', action: 'createForPhoneNumber')}.json";
	var dataURL = "${createLink(controller:'turn', action: 'listByQueueAndDatetimeRange')}.json";
	var statusURL = "${createLink(controller:'fixedTimeTurn', action: 'update')}";
	var notifyURL = "${createLink(controller:'turn', action: 'getTurnRegistrationNotification')}.json";
	var queue_id = ${user_queue_id};
	var processing = false;
	var polling = false;
	var pollTime = 30; //seconds for each ajax call to update notifications
	var dateFrom, dateThru, now, hour, gmtOffset, jqDatatable;

	</script>
  </body>
</html>