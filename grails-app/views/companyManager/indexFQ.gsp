<%@page import="org.apache.shiro.SecurityUtils"%>
<%@page import="grails.plugin.nimble.core.UserBase"%>
<!DOCTYPE html>
<html>
  <head>
    <meta name="layout" content="app" />
    <title>TuTurno | <g:message code="default.turn.management"/></title>
	<link rel="stylesheet" href="${resource(dir: 'css', file: 'datepicker.css')}" type="text/css">
	<link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap-timepicker.min.css')}" type="text/css">
	<link rel="stylesheet" href="${resource(dir: 'css', file: 'dataTables.bootstrap.css')}" type="text/css">
	<link rel="stylesheet" href="//cdn.datatables.net/tabletools/2.2.1/css/dataTables.tableTools.css" type="text/css">
  </head>
  <body class="companyManager">
    <g:set var="currentUser" value="${UserBase.get(SecurityUtils.subject.principal)}" />
    <div class="row">
        <div id="main-content" class="col-lg-12">
            <h1><g:message code="default.turn.management"/> - ${currentUser.company}</h1>
			<div class="row">
				<div class="col-lg-4 col-md-4"><!-- Start table 1st column -->
					<div class="panel panel-primary">
					  <div class="panel-heading"><g:message code="turn.management.assignturn"/></div>
					  <div class="panel-body">
						<form role="form">
							<div class="form-group">
								<label class="control-label"><g:message code="turn.management.phone.number"/></label>
								<input type="text" class="form-control" id="subscriber" name="subscriber">
							</div>
							<div class="form-group">
								<label class="control-label"><g:message code="turn.management.date.time"/></label>								
								<div class="input-group input-append bootstrap-timepicker">
									<input id="timepicker" type="text" class="input-small form-control">
									<span class="input-group-addon add-on"><i class="glyphicon glyphicon-time icon-time"></i></span>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label"><g:message code="turn.management.date.date"/></label>
								<div class="input-group date">
									<input id="datepicker" type="text" class="form-control" readonly="readonly"
										value="<g:formatDate format="dd/MM/yyyy" date="${new Date()}"/>"/>
									<span class="input-group-addon add-on"><i class="glyphicon glyphicon-th"></i></span>
								</div>
							</div>
							<button id="turn-submit" type="button" class="btn btn-primary"><g:message code="turn.management.assignturn"/></button>
							<span id="ajax-loader"></span>
						</form>
					  </div>
					</div>
				</div><!-- End table 1st column -->
				
				<div class="col-lg-8 col-md-8"><!-- Start table 2d column -->
					<div id="turns-panel" class="panel panel-primary">
						<div class="panel-heading"><g:message code="turn.management.assignedturns"/></div>
						<div class="panel-body ">
							<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="dataTable-turnos">
							<thead>
							  <tr>
								<th># <i class="fa fa-sort"></i></th>
								<th>id</th>
								<th><g:message code="turn.management.phone.number"/></th>
								<th><g:message code="turn.management.date.date"/></th>
								<th><g:message code="turn.management.date.time"/></th>
								<th><g:message code="turn.management.status"/></th>
								<th title="Estado de Notificación al Celular"><g:message code="turn.management.notify"/></th>
							  </tr>
							</thead>
							</table>
							</div>
							<hr />
						</div>
						

					</div>
				
				</div><!-- end table 2d column -->
			</div>        </div>
    </div><!-- end-row -->
	
	<script src="http://cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
	<script src="http://cdn.datatables.net/tabletools/2.2.1/js/dataTables.tableTools.min.js"></script>
	<g:javascript src="timepicker/bootstrap-timepicker.js"></g:javascript>	
	<g:javascript src="datepicker/bootstrap-datepicker.js"></g:javascript>
	<g:javascript src="datepicker/bootstrap-datepicker.es.js"></g:javascript>
	<g:javascript src="dataTables/dataTables.bootstrap.js"></g:javascript>
	<g:javascript src="i18n/jquery.i18n.min.js"></g:javascript>
	<g:javascript src="moment.min.js"></g:javascript>
	
	<script type="text/javascript">	
	var saveURL = "${createLink(controller:'fixedTimeTurn', action: 'createForPhoneNumber')}.json";
	var dataURL = "${createLink(controller:'turn', action: 'listByQueueAndDatetimeRange')}.json";
	var statusURL = "${createLink(controller:'fixedTimeTurn', action: 'update')}";
	var notifyURL = "${createLink(controller:'turn', action: 'getTurnRegistrationNotification')}.json";
	var queue_id = ${user_queue_id};
	var processing = false;
	var polling = false;
	var pollTime = 30; //seconds for each ajax call to update notifications
	var dateFrom, dateThru, now, hour, gmtOffset, jqDatatable;
	
	initDates(); // configure current dates	
	updateTableTitle(dateFrom, dateThru); // upate center panel header to show the dates as title
	
	var turnsTable = null;
	var rowCount = 1;
	var lastId = 0;
	if (typeof jQuery !== 'undefined') {
		(function($) {
			$( document ).ajaxError(function(event, request, settings) {
			  $( "#main-content" ).prepend( "<div class='has-error'><label class='control-label'>Error requesting url " + settings.url + "</label></div>" );
			});
			
			turnsTable = jQuery('#dataTable-turnos').DataTable( {
				language: {url: 'http://cdn.datatables.net/plug-ins/28e7751dbec/i18n/Spanish.json'},
				"columns": [
					{ data: 'row' },
					{ data: 'id' },
					{ data: 'number' },
					{ data: 'date' },
					{ data: 'time' },
					{ data: 'status' },
					{ data: 'notify' }
				],
				"columnDefs": [ { "targets": [ 1 ], "visible": false, "searchable": false } ],
				"ajax": {
					"url": dataURL + "?queue="+queue_id+"&from="+dateFrom+"&thru="+dateThru,
					"dataSrc": function ( json ) {
					  var row_count = json.length;
					  for ( var i=0, ien=row_count; i<ien ; i++ ) {
						json[i] = adapterTurnResponse(json[i]);
					  }
					  return json;
					}
				},
				"fnDrawCallback": function( oSettings ) {
					initPollNotifications();
					
					if(lastId===0) {
						return true;
					}
					// Highlight row effect for the last turn created!! :)
					var firstTr = $('#dataTable-turnos tbody').find('tr').eq(0);
					firstTr.addClass('success');					
					setTimeout( removeSuccessClass, 500 );
					
				},
				"createdRow": function ( row, data, index ) {
					var lastCol = $(row).find('td').eq(4);
					lastCol.html( '<span id="status'+data.id+'" class="status-text">' + lastCol.html() + '</span>' );
					
					var linkEdit = $('<a>', {'class':'editRow', href:'#', onclick:'showEditStatus('+data.id+');return false'});
					var img = $('<i>', {'class':'glyphicon glyphicon-edit icon-edit'});
					linkEdit.append( img );
					lastCol.append( linkEdit );
					
					var linkSave = $('<a>', {'class':'saveRow', href:'#', onclick:'saveStatus('+data.id+');return false'});
					var imgSave = $('<i>', {'class':'glyphicon glyphicon-ok icon-ok'});
					linkSave.hide();
					linkSave.append(imgSave);
					lastCol.append( linkSave );
				}
			});
			turnsTable.column( '0:visible' ).order( 'desc' );
			var tableTools = new $.fn.dataTable.TableTools( turnsTable, {
				"buttons": [
					"copy",
					"csv",
					"xls",
					"pdf",
					{ "type": "print", "buttonText": "Print me!" }
				]
			} );
			$( tableTools.fnContainer() ).insertAfter('div.table-responsive');
			
			var dp = $('.form-group .date').datepicker({
				format: "dd/mm/yyyy",
				startDate: moment(dateFrom).format('DD/MM/YYYY'),
				endDate: moment(dateThru).format('DD/MM/YYYY'),
				todayBtn: "linked",
				language: "es",
				autoclose: true,
				todayHighlight: true
			});
			
			var tp = $('#timepicker').timepicker({defaultTime: 'current', minuteStep:5});			
			
			$('#turn-submit').click(createNewTurn);			
		
		})(jQuery);
		
		function updateTableTitle() {
			var title = $('#turns-panel .panel-heading').html();
			var df = moment( dateFrom );
			var dt = moment( dateThru );
			
			title += "<span class='title-date pull-right'>" + df.format('MMMM DD [de] YYYY') + " - " + dt.format('MMMM DD [de] YYYY') + " </span>";
			
			$('#turns-panel .panel-heading').html( title );
		}
		
		function initDates() {
			var userLang = navigator.language || navigator.userLanguage; 
			
			//Define global language
			moment.lang( userLang );
			
			langConfig = { directory: "${resource(dir: 'js', file: 'locale')}", locale: userLang };
			$.ajax({
				url: langConfig.directory + "/" + langConfig.locale + ".json",
				async: false,
				success: function(data) {
					$.i18n.load(data);
				}
			});
			
			now = new Date();
			gmtOffset = now.getTimezoneOffset();
			
			hour = now.getHours();
			var string_hour = (hour<10) ? '0'+hour : ''+hour;
			
			// fix the GMT offset for the current datetime
			now = new Date(now.getTime() - (gmtOffset*60000));
			
			// if current Hour is between 12 & 24,
			if(hour>=12 && hour<24) {		
				// -> from=today  &  thru=tomorrow
				dateFrom = now.toISOString();
				now.setDate(now.getDate() + 1);
				dateThru = now.toISOString();
			}
			// else if the current Hour is beteween 00 & 12
			else  {
				// -> from=yesterday  &  thru=today
				//     TODO: on the yesterday turns, it is needed to validate only if the hour is > 12		
				dateThru = now.toISOString();
				now.setDate(now.getDate() - 1);
				dateFrom = now.toISOString();
			}
			
			dateThru = dateThru.replace('T'+string_hour+':', 'T12:');
			dateFrom = dateFrom.replace('T'+string_hour+':', 'T12:');
		}
		
		
		function showEditStatus(turnID) {
			var statusTag = $('tr#'+turnID).find('.status-text');
			
			var updater = $('<select>', {id:'statusSelect'+turnID, 'class':'status-dropdown'});
			updater.append('<option value="pending">'+$.i18n._('pending')+'</option>');
			updater.append('<option value="attended">'+$.i18n._('attended')+'</option>');
			updater.append('<option value="cancelled">'+$.i18n._('cancelled')+'</option>');
			
			statusTag.hide();
			var p = statusTag.parent();
			p.prepend(updater);
			
			$('tr#'+turnID).find('.editRow').hide();
			$('tr#'+turnID).find('.saveRow').show();
		}
		
		function saveStatus(turnID) {
			var newStatus = $('#statusSelect'+turnID).val();
			var textStatus = $('#statusSelect'+turnID+' :selected').text();
			
			var data = {status:newStatus};
			$.ajax({
			  type: "POST",
			  url: statusURL+"/"+turnID+".json",
			  data: JSON.stringify(data),
			  dataType: 'json',
			  contentType: 'application/json',
			  success: callbackUpdateTurnStatus
			});
			
			$('tr#'+turnID).find('.status-text').html( textStatus );			
		}
		
		function callbackUpdateTurnStatus(data, textStatus, xhr) {
			$('#statusSelect'+data.id).remove();
			$('tr#'+data.id).find('.saveRow').hide();
			$('tr#'+data.id).find('.editRow').fadeIn();
			$('tr#'+data.id).find('.status-text').fadeIn();
		}
		
		
		/**
		* turn-submit button event:
		* Validate all inputs, calculate the correct datetimes,
		* pack the turn into a json object and send it to the server to save it
		*
		**/
		function createNewTurn() {
			//validation for double click or users with hurry
			if(processing) return false;
			
			var _phone = $('#subscriber');
			var _time = $('#timepicker');
			var _date = $('#datepicker');
			
			if( isEmpty(_phone) || isEmpty(_time) || isEmpty(_date) ) {
				return false;
			} else {
				_phone.parent().removeClass('has-error');
				_time.parent().removeClass('has-error');
				_date.parent().removeClass('has-error');					
			}
		
			dates = _date.val().split('/');
			//split by blank spaces and colons
			times = _time.val().split(/[ ,\/:]/);
			var d = new Date(dates[1]+'-'+dates[0]+'-'+dates[2]+" "+_time.val());
			
			var datetime = d.toISOString();
			datetime = datetime.replace('.000', '');
			
			processing = true;
			$('#ajax-loader').addClass('spinner');
			
			var turn = { queue: queue_id, phoneNumber:_phone.val(), datetime:datetime, status:'pending' }
			$.ajax({
			  type: "POST",
			  url: saveURL,
			  data: JSON.stringify(turn),
			  success: callbackSaveTurn,
			  dataType: 'json',
			  contentType: 'application/json'
			});
			
		}


		function adapterTurnResponse(json) {
		
			var d = moment(json[0].datetime);
			date = d.format('L');
			time = d.format('h:mm a');

			json.row = rowCount++;
			json.id = json[0].id;
			json.DT_RowId = json[0].id;			
			json.number = json[1];
			json.date = date;
			json.time = time;
			json.status = $.i18n._( json[0].status );
			$.ajax({
				url: notifyURL+"?turn="+json.id,
				async: false,
				success: function(data) {
					json.notify = "<span title='"+$.i18n._( 'notification_'+data.statusCode )+"' class='notification-image "+data.statusCode+"'>&nbsp;</span>";
				},
				error: function() {
					json.notify = "-N/A-";
				}
			});

			return json;
		}
		
		function isEmpty(_field){
			if(_field.val().length===0) {
				_field.parent().addClass('has-error');
				return true;
			}
			return false;
		}

		function callbackSaveTurn(turn) {
			rowCount = 1;
			lastId=turn.id;
			turnsTable.ajax.reload();
			
			processing = false;
			$('#ajax-loader').removeClass('spinner');
			$('#subscriber').val('').focus();
		}
		
		function removeSuccessClass()  {
			firstTr = $('#dataTable-turnos tbody').find('tr').eq(0).removeClass('success');
		}
		
		/** This function will start a plling to keep updated the notifications state **/
		function initPollNotifications() {
		
			if( polling==true ) return false;
			
			jqDatatable = jQuery('#dataTable-turnos');
			var rows = jqDatatable.find('tbody tr');
			if( rows.text()=='Cargando...' ) {
				setTimeout( initPollNotifications, pollTime*1000 );
				return false;
			}
			
			polling = true;
			console.log( 'starting polling' );
			
			setInterval(function() {
				rows.each(function(index) {
					var turnId = jQuery(this).attr('id');
					var span = jQuery(this).find('.notification-image');
					
					if( span.hasClass('pending') ) {
						$.ajax({
							url: notifyURL+"?turn="+turnId,
							success: function(data) {
								var row = jqDatatable.find('#'+data.id);
								var span = row.find('.notification-image');
								span.attr({title: $.i18n._( 'notification_'+data.statusCode ) });
								span.removeClass();
								span.addClass( 'notification-image' );
								span.addClass( data.statusCode );
							},
							error: function() {
								json.notify = "-- error --";
							}
						});
					}
				});
			}, pollTime*1000);
		}
	};
	</script>
  </body>
</html>