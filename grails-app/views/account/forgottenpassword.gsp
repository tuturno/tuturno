<!doctype html>
<html>
   <head>
      <title>
         <g:message code="nimble.view.account.forgottenpassword.initiate.title" />
      </title>
      <r:require modules="nimble-login" />
	  <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:300,400,700" type="text/css">
      <r:layoutResources />
   </head>
	<body class="login">
		<div id="main-welcome" class="row-fluid">
			<!-- First Col -->
			<div id="left-col" class="span6">
				<img alt="UsuarioApp" src="http://tuturnoapp.com/img/EmpresaGrap.png" class="box-image"/>
				<div class="text">
					<h2>Maricadas</h2>
					<p>Como para publicidad, mensajes, noticias o cositas de interes</p>
				</div>
			</div>
			<!-- end first col-->
			
			<!-- Second Col -->
			<div id="right-col" class="span5">
			  <div class="login-container">
				 <div class="login-content">
					<g:link controller="Home">
						<img alt="Logo TuTurno" src="http://www.tuturnoapp.com/img/logoAppTuTurno.png" id="logo" />
					</g:link>
					<h4 class="title border-bottom"><g:message code="nimble.view.account.forgottenpassword.initiate.heading" /></h4>
					
					<n:flashembed />
					<div class="login-options">
					<span>
					   <g:message code="nimble.view.account.forgottenpassword.initiate.descriptive" />
					</span>         
					</div>               
					<g:form action="forgottenpasswordprocess" method="POST">
					   <div class="login-input">
						  <div class="control-group">
							 <div class="controls "> 
								<input type="text" size="30" name="email" id="email" placeholder="usuario@ejemplo.com">
							 </div>
						  </div>
					   </div>
					   <div class="login-actions">
						  <span>
						  <button type="submit" class="btn btn-primary">
							 <g:message code="nimble.link.resetpassword" />
						  </button>
						  </span>
					   </div>
					</g:form>
				 </div>
			  </div>
			</div>
			<!-- end second col-->
		</div>
      <r:layoutResources />
   </body>
</html>