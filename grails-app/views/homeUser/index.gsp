<%@page import="org.apache.shiro.SecurityUtils"%>
<%@page import="grails.plugin.nimble.core.UserBase"%>
<!DOCTYPE html>
<html>
  <head>
    <meta name="layout" content="app" />
    <title>TuTurno | <g:message code="default.home.label"/></title>
  </head>
  <body>
    <g:set var="currentUser" value="${UserBase.get(SecurityUtils.subject.principal)}" />
    <g:if test="${session.getAttribute('app.tuturno.queueType') == 'FQ'}">
      <g:render template="stats-fq" />
    </g:if>
    <g:if test="${session.getAttribute('app.tuturno.queueType') == 'NQ'}">
      <g:render template="stats-nq" />
    </g:if>

  </body>
</html>
