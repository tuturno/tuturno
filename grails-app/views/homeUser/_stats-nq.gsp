<div class="container-fluid">

    <div class="row">
      <div class="col-lg-12">
        <h1>${currentUser.company} <small>Hola  ${currentUser.profile.fullName} </small></h1>
        <div class="alert alert-success alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          Te presentamos el nuevo panel de administración de tus turnos, con gestión de estadísticas, tráfico de las apps y demás contenidos relevantes para la gestión de la plataforma TuTurno!
        </div>
      </div>
    </div><!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Tiempo promedio de atención de turnos (minutos)</h3>
                </div>
                <div class="panel-body">
                    <div id="morris-chart-area"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> Promedio semanal de atención de turnos</h3>
            </div>
            <div class="panel-body">
                <div id="morris-chart-line"></div>
                <div class="text-right">
                  <a href="#">Ver Detalles <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> Cantidad semanal de turnos atendidos</h3>
            </div>
            <div class="panel-body">
                <div id="morris-chart-bar"></div>
                <div class="text-right">
                  <a href="#">Ver Detalles <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            </div>
        </div>
    </div><!-- /.row -->

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-warning">
              <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> Resumen mensual de Canal de Asignación de Turnos</h3>
              </div>
              <div class="panel-body">
                <div class="flot-chart">
                  <div class="flot-chart-content" id="flot-chart-multiple-axes"></div>
                </div>
                <div class="text-right">
                  <a href="#">Ver Detalles <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              </div>
            </div>
          </div>
    </div>

</div>
<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script>
<g:javascript src="charts/morris/chart-data-morris.js"></g:javascript>

<g:javascript src="charts/flot/jquery.flot.js"></g:javascript>
<g:javascript src="charts/flot/jquery.flot.tooltip.min.js"></g:javascript>
<g:javascript src="charts/flot/jquery.flot.resize.js"></g:javascript>
<g:javascript src="charts/flot/chart-data-flot.js"></g:javascript>