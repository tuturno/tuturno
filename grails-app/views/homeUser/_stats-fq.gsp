<div class="row">
  <div class="col-lg-12">
    <h1>${currentUser.company} <small>Hola  ${currentUser.profile.fullName} </small></h1>
    <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      Nunca antes la gestion de turnos había sido tan fácil!!  Te presentamos el nuevo panel de administración de tus turnos, con gestión de estadísticas, tráfico de las apps y demás contenidos relevantes para la gestión de la plataforma TuTurno!
    </div>
  </div>
</div><!-- /.row -->

<div class="col-lg-12 row">
  <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
    <div class="panel panel-info">
      <div class="panel-heading">
        <div class="row">
          <div class="col-xs-5"><i class="fa fa-comments fa-5x"></i></div>
          <div class="col-xs-7 text-right">
            <p class="announcement-heading">156</p>
            <p class="announcement-text">Nuevos usuarios!</p>
          </div>
        </div>
      </div>
      <a href="#">
        <div class="panel-footer announcement-bottom">
          <div class="row">
            <div class="col-xs-8">Ver registros</div>
            <div class="col-xs-4 text-right"><i class="fa fa-arrow-circle-right"></i></div>
          </div>
        </div>
      </a>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
    <div class="panel panel-success">
      <div class="panel-heading">
        <div class="row">
          <div class="col-xs-5"><i class="fa fa-comments fa-5x"></i></div>
          <div class="col-xs-7 text-right">
            <p class="announcement-heading">56</p>
            <p class="announcement-text">Usuarios Activos</p>
          </div>
        </div>
      </div>
      <a href="#">
        <div class="panel-footer announcement-bottom">
          <div class="row">
            <div class="col-xs-8">Ver Compañias</div>
            <div class="col-xs-4 text-right"><i class="fa fa-arrow-circle-right"></i></div>
          </div>
        </div>
      </a>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
    <div class="panel panel-warning">
      <div class="panel-heading">
        <div class="row">
          <div class="col-xs-5"><i class="fa fa-check fa-5x"></i></div>
          <div class="col-xs-7 text-right">
            <p class="announcement-heading">120</p>
            <p class="announcement-text">Turnos Activos</p>
          </div>
        </div>
      </div>
      <a href="#">
        <div class="panel-footer announcement-bottom">
          <div class="row">
            <div class="col-xs-8">Ver Turnos</div>
            <div class="col-xs-4 text-right"><i class="fa fa-arrow-circle-right"></i></div>
          </div>
        </div>
      </a>
    </div>
  </div>
  <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
    <div class="panel panel-danger">
      <div class="panel-heading">
        <div class="row">
          <div class="col-xs-5"><i class="fa fa-tasks fa-5x"></i></div>
          <div class="col-xs-7 text-right">
            <p class="announcement-heading">1</p>
            <p class="announcement-text">Caidas de conexión</p>
          </div>
        </div>
      </div>
      <a href="#">
        <div class="panel-footer announcement-bottom">
          <div class="row">
            <div class="col-xs-8">Ver logs</div>
            <div class="col-xs-4 text-right"><i class="fa fa-arrow-circle-right"></i></div>
          </div>
        </div>
      </a>
    </div>
  </div>
</div><!-- /.row -->
