<!DOCTYPE html>
<%@page import="org.apache.shiro.SecurityUtils"%>
<%@page import="grails.plugin.nimble.core.UserBase"%>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="TuTurno"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap3.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'sb-admin.css')}" type="text/css">
    <link rel="stylesheet" href="${resource(dir: 'font-awesome/css', file: 'font-awesome.min.css')}" type="text/css">
    <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">

		<g:layoutHead/>
		<script src="${resource(dir: 'js', file: 'jquery-1.10.2.js')}"></script>
		<g:javascript library="application"/>		
		<r:layoutResources />
	</head>
	<body>
      <g:set var="currentUser" value="${UserBase.get(SecurityUtils.getSubject()?.getPrincipal())}" />
      <div id="wrapper">
         <!-- Sidebar -->
         <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
             </button>
             <g:link controller="Home" class="navbar-brand">
               <img alt="Log TuTurno" src="http://tuturnoapp.com/img/TuTurnoLogo.png" />
             </g:link>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
               <div class="collapse navbar-collapse navbar-ex1-collapse">
                  <g:render template="/templates/nimble/navigation/appnavigation" />

                <ul class="nav navbar-nav navbar-right navbar-user">                  
                  <li class="dropdown user-dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-user"></i> ${currentUser.username} <b class="caret"></b>
                     </a>
                    <ul class="dropdown-menu">
                        <!-- <li>
                           <g:link >
                              <i class="fa fa-user"></i> <g:message code="nimble.link.myaccount" /></a>
                           </g:link>
                        </li>
                        <li class="divider"></li>-->
                        <li><a href="#">
                           <g:link controller="remoteAuthController" action="localSignout">
                              <i class="fa fa-power-off"></i> <g:message code="nimble.link.logout.basic" /></a>
                           </g:link>
                        </li>
                    </ul>
                  </li>
                </ul>
              </div><!-- /.navbar-collapse -->
         </nav>

         <div id="page-wrapper">

           <div class="row">
             <div class="col-lg-12">
               <g:layoutBody/>
             </div>
           </div><!-- /.row -->

         </div><!-- /#page-wrapper -->

      </div><!-- /#wrapper -->
		

		<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
      <!-- JavaScript -->
      <script src="${resource(dir: 'js', file: 'bootstrap3.min.js')}"></script>

		<r:layoutResources />
	</body>
</html>
