<!doctype html>
<%@page import="org.apache.shiro.SecurityUtils"%>
<%@page import="grails.plugin.nimble.core.UserBase"%>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <g:layoutHead/>
      <link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
      <link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
      <link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
      
      <link rel="stylesheet" href="${resource(id:'jgrawl', file: 'bundle-bundle_jgrawl_head.css')}" type="text/css" />
      <link rel="stylesheet" href="${resource(plugin:'nimble', dir:'css', file:'nimble-layout.css')}" type="text/css" />
      <link rel="stylesheet" href="${resource(plugin:'nimble', dir:'css', file:'nimble-widgets.css')}" type="text/css" />
      <link rel="stylesheet" href="${resource(plugin:'nimble', dir:'css', file:'nimble-typography.css')}" type="text/css" />
      <link rel="stylesheet" href="${resource(plugin:'nimble', dir:'css', file:'nimble-core.css')}" type="text/css" />
      <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap3.css')}" type="text/css">
      <link rel="stylesheet" href="${resource(dir: 'css', file: 'grails-styles.css')}" type="text/css">
      <link rel="stylesheet" href="${resource(dir: 'font-awesome/css', file: 'font-awesome.min.css')}" type="text/css">
      <link rel="stylesheet" href="${resource(dir: 'css', file: 'sb-admin.css')}" type="text/css">
      <g:javascript library="application"/>     
      <r:layoutResources />
   </head>
   <body class="tt">
      <g:set var="currentUser" value="${UserBase.get(SecurityUtils.subject.principal)}" />
      <div id="wrapper">
         <!-- Sidebar -->
         <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
             </button>
             <g:link controller="Home" class="navbar-brand">
               <img alt="Log TuTurno" src="http://tuturnoapp.com/img/TuTurnoLogo.png" />
             </g:link>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
               <div class="collapse navbar-collapse navbar-ex1-collapse">
                  <g:render template="/templates/nimble/navigation/sidenavigation" />

                <ul class="nav navbar-nav navbar-right navbar-user">                  
                  <li class="dropdown user-dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-user"></i> ${currentUser?.username} <b class="caret"></b>
                     </a>
                    <ul class="dropdown-menu">
                        <li>
                           <g:link controller="user" action="show" id="${currentUser?.id}">
                              <i class="fa fa-user"></i> <g:message code="nimble.link.myaccount" /></a>
                           </g:link>
                        </li>
                        <li class="divider"></li>
                        <li><a href="#">
                           <g:link controller="auth" action="signout">
                              <i class="fa fa-power-off"></i> <g:message code="nimble.link.logout.basic" /></a>
                           </g:link>
                        </li>
                    </ul>
                  </li>
                </ul>
              </div><!-- /.navbar-collapse -->
         </nav>

         <div id="page-wrapper">

           <div class="row">
             <div class="col-lg-12 main-content">
               <g:layoutBody />
             </div>
           </div><!-- /.row -->

         </div><!-- /#page-wrapper -->

      </div><!-- /#wrapper -->


      <!-- Main container ends -->
      <%--  <n:sessionterminated /> --%>
      <!-- JavaScript -->
      <script src="${resource(dir: 'js', file: 'jquery-1.10.2.js')}"></script>
      <script src="${resource(dir: 'js', file: 'bootstrap3.min.js')}"></script>

      <script src="${resource(id:'jgrawl', file: 'bundle-bundle_jgrawl_head.js')}" type="text/javascript" ></script>
      <script src="${resource(plugin:'nimble', dir:'js', file:'nimblecore.js')}" type="text/javascript" ></script>
      <script src="${resource(plugin:'nimble', dir:'js', file:'nimbleui.js')}" type="text/javascript" ></script>
      <script src="${resource(plugin:'nimble', dir:'js/jquery', file:'jquery.pstrength.js')}" type="text/javascript" ></script>
      <r:layoutResources />
   </body>
</html>