package app.tuturno

class StatUtils {

	def static average(data) {
		if (!data) {
			return 0
		}

		def average = 0

		for (val in data) {
			average += val
		}

		return average / data.size()
	}

	def static variance(data, dataAverage) {
		if (!data) {
			return 0
		}

		def variance = average(data.collect { Math.pow(it - dataAverage, 2) })
		return variance
	}

	def static standardDeviation(data, dataAverage) {
		return Math.sqrt(variance(data, dataAverage))
	}
}
