package app.tuturno

class TurnUtils {

	def static calculateNextTurnToAssign(queue) {
    	queue.lastAssignedTurn == queue.maxTurn ? 1 : queue.lastAssignedTurn + 1
    }
	
	def static calculateNextTurnCall(queue) {
		queue.currentTurn == queue.maxTurn ? 1 : queue.currentTurn + 1
	}

    def static calculateTurnCallsTimeLapseAverage(turnCalls) {
    	// Calculate time lapses
    	def timeLapses = calculateTimeLapses(turnCalls)
		
		if (!timeLapses) {
			return 0
		}
		
		// Calculate standard deviation for filtering time lapses, discarding uncommon time lapses
		def average = StatUtils.average(timeLapses)
		def filteredTimeLapses = filterTimeLapses(timeLapses, average, StatUtils.standardDeviation(timeLapses, average))

    	return StatUtils.average(filteredTimeLapses) / 1000
    }
	
	def static calculateTimeLapses(turnCalls) {
		def timeLapses = []
		
		for (int i = 0; i < turnCalls.size() - 1; i++) {
			if (turnCalls[i].jumpElapsedTimeCalculation == false) {
				timeLapses << turnCalls[i].dateCreated.time - turnCalls[i + 1].dateCreated.time
			}
		}
		
		return timeLapses
	}
	
	def static filterTimeLapses(timeLapses, average, standardDeviation) {
		def filteredTimeLapses = []
		
		timeLapses.each { timeLapse ->
			if (Math.abs(timeLapse - average) <= standardDeviation) {
				filteredTimeLapses << timeLapse
			}
		}
		
		return filteredTimeLapses
	}
}
