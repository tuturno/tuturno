package app.tuturno



import grails.test.mixin.*
import spock.lang.*

@TestFor(FixedTimeTurnController)
@Mock(FixedTimeTurn)
class FixedTimeTurnControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.fixedTimeTurnInstanceList
            model.fixedTimeTurnInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.fixedTimeTurnInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            def fixedTimeTurn = new FixedTimeTurn()
            fixedTimeTurn.validate()
            controller.save(fixedTimeTurn)

        then:"The create view is rendered again with the correct model"
            model.fixedTimeTurnInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            fixedTimeTurn = new FixedTimeTurn(params)

            controller.save(fixedTimeTurn)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/fixedTimeTurn/show/1'
            controller.flash.message != null
            FixedTimeTurn.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def fixedTimeTurn = new FixedTimeTurn(params)
            controller.show(fixedTimeTurn)

        then:"A model is populated containing the domain instance"
            model.fixedTimeTurnInstance == fixedTimeTurn
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def fixedTimeTurn = new FixedTimeTurn(params)
            controller.edit(fixedTimeTurn)

        then:"A model is populated containing the domain instance"
            model.fixedTimeTurnInstance == fixedTimeTurn
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/fixedTimeTurn/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def fixedTimeTurn = new FixedTimeTurn()
            fixedTimeTurn.validate()
            controller.update(fixedTimeTurn)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.fixedTimeTurnInstance == fixedTimeTurn

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            fixedTimeTurn = new FixedTimeTurn(params).save(flush: true)
            controller.update(fixedTimeTurn)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/fixedTimeTurn/show/$fixedTimeTurn.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/fixedTimeTurn/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def fixedTimeTurn = new FixedTimeTurn(params).save(flush: true)

        then:"It exists"
            FixedTimeTurn.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(fixedTimeTurn)

        then:"The instance is deleted"
            FixedTimeTurn.count() == 0
            response.redirectedUrl == '/fixedTimeTurn/index'
            flash.message != null
    }
}
