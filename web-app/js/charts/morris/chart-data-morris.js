// First Chart Example - Area Line Chart

Morris.Area({
  // ID of the element in which to draw the chart.
  element: 'morris-chart-area',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.
  data: [
	{ d: '2014-10-10 08:12', tiempo: 19 },
	{ d: '2014-10-10 08:31', tiempo: 18 },
	{ d: '2014-10-10 08:47', tiempo: 20 },
	{ d: '2014-10-10 09:10', tiempo: 24 },
	{ d: '2014-10-10 09:19', tiempo: 19 },
	{ d: '2014-10-10 09:41', tiempo: 21 },
	{ d: '2014-10-10 10:00', tiempo: 19 },
	{ d: '2014-10-10 10:17', tiempo: 18 },
	{ d: '2014-10-10 10:40', tiempo: 23 },
	{ d: '2014-10-10 11:05', tiempo: 25 },
	{ d: '2014-10-10 11:27', tiempo: 22 },
	{ d: '2014-10-10 11:49', tiempo: 22 },
	{ d: '2014-10-10 12:10', tiempo: 20 },
	{ d: '2014-10-10 12:29', tiempo: 19 },
	{ d: '2014-10-10 12:48', tiempo: 20 },
	{ d: '2014-10-10 12:56', tiempo: 18 },
	{ d: '2014-10-10 13:17', tiempo: 21 },
	{ d: '2014-10-10 13:40', tiempo: 24 },
	{ d: '2014-10-10 14:06', tiempo: 26 },
	{ d: '2014-10-10 14:30', tiempo: 26 },
	{ d: '2014-10-10 14:54', tiempo: 24 },
	{ d: '2014-10-10 15:16', tiempo: 22 },
	{ d: '2014-10-10 15:39', tiempo: 23 },
	{ d: '2014-10-10 16:02', tiempo: 24 },
	{ d: '2014-10-10 16:24', tiempo: 22 },
	{ d: '2014-10-10 16:55', tiempo: 21 },
	{ d: '2014-10-10 17:16', tiempo: 21 },
	{ d: '2014-10-10 17:38', tiempo: 22 },
	{ d: '2014-10-10 17:57', tiempo: 19 },
	{ d: '2014-10-10 18:17', tiempo: 20 },
	{ d: '2014-10-10 18:38', tiempo: 21 }
  ],
  // The name of the data record attribute that contains x-visitss.
  xkey: 'd',
  // A list of names of data record attributes that contain y-visitss.
  ykeys: ['tiempo'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['Tiempo'],
  // Disables line smoothing
  smooth: false
});


Morris.Line({
  // ID of the element in which to draw the chart.
  element: 'morris-chart-line',
  // Chart data records -- each entry in this array corresponds to a point on
  // the chart.
  data: [
	{ d: '2014-10-10 08:12', tiempo: 19 },
	{ d: '2014-10-10 08:47', tiempo: 20 },
	{ d: '2014-10-10 09:19', tiempo: 19 },
	{ d: '2014-10-10 10:00', tiempo: 19 },
	{ d: '2014-10-10 10:40', tiempo: 23 },
	{ d: '2014-10-10 11:27', tiempo: 22 },
	{ d: '2014-10-10 12:10', tiempo: 20 },
	{ d: '2014-10-10 12:48', tiempo: 20 },
	{ d: '2014-10-10 13:17', tiempo: 21 },
	{ d: '2014-10-10 14:06', tiempo: 20 },
	{ d: '2014-10-10 14:54', tiempo: 24 },
	{ d: '2014-10-10 15:39', tiempo: 23 },
	{ d: '2014-10-10 16:24', tiempo: 22 },
	{ d: '2014-10-10 17:16', tiempo: 21 },
	{ d: '2014-10-10 17:57', tiempo: 19 },
	{ d: '2014-10-10 18:38', tiempo: 21 }
  ],
  // The name of the data record attribute that contains x-visitss.
  xkey: 'd',
  // A list of names of data record attributes that contain y-visitss.
  ykeys: ['tiempo'],
  // Labels for the ykeys -- will be displayed when you hover over the
  // chart.
  labels: ['Tiempo'],
  // Disables line smoothing
  smooth: false,
});

Morris.Bar ({
  element: 'morris-chart-bar',
  data: [
	{device: 'Lunes', turnos: 156},
	{device: 'Martes', turnos: 197},
	{device: 'Miercoles', turnos: 255},
	{device: 'Jueves', turnos: 280},
	{device: 'Viernes', turnos: 255},
	{device: 'Sábado', turnos: 331},
	{device: 'Domingo', turnos: 371}
  ],
  xkey: 'device',
  ykeys: ['turnos'],
  labels: ['Turnos'],
  barRatio: 0.4,
  xLabelAngle: 35,
  hideHover: 'auto'
});
